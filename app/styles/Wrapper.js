import {StyleSheet} from 'react-native';
import React from 'react';
const Colors  = require('../source/colors.json');

const Wrapper  = {
  menu: {
      flex: 0,
      height:60
  },
  header:{
    height:50,
    backgroundColor:Colors.blue.blue800,
    paddingBottom:10,
    paddingLeft:10,
    paddingRight:10
  },
  subheader:{
    height:50,
    backgroundColor:Colors.blue.blue400,
    paddingBottom:10
  },
  catlist :{
    height:45, 
    borderWidth:0.5,
    borderColor:Colors.grey.grey500,
    backgroundColor: Colors.deepPurple.deepPurple400,
    justifyContent:'center',
    paddingLeft:5,
    paddingRight:5,
    paddingTop:10,
    flexDirection:'row'
  },
  subcatlist :{
    height:45, 
    borderWidth:0.5,
    borderColor:Colors.grey.grey500,
    backgroundColor: Colors.deepOrange.deepOrange600,
    justifyContent:'center',
    paddingLeft:5,
    paddingRight:5,
    paddingTop:10,
    flexDirection:'row'
  },
  boxlist :{
    height:120, 
    borderWidth:0.5,
    borderColor:Colors.grey.grey500,
    backgroundColor: 'white',
    justifyContent:'center',
    paddingLeft:5,
    paddingRight:5,
    flexDirection:'row',
    flexWrap: 'wrap',
    marginTop:5
  },
  boxlist2: {
    height: 'auto',
    borderWidth: 0.5,
    borderColor: Colors.grey.grey500,
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    padding:5
  },
  listtext:{
    fontSize:15,
    color:'white',
    flex:7
  }
};


export default Wrapper;
