import React, { Component } from 'react';
import { ActivityIndicator, ListView, Text, View, NetInfo } from 'react-native';
import Utilities from '../Components/Utilities'

const config = require('../source/config.json')
const SITENAME = config.sitename;

const FetchHtml = {
  getData : getData,
  getGroup: getGroup,
  getComm : getComm,
  getRawData: getRawData,
  getDataGET:getDataGET
}

async function getData(url) {
  let isNet = await getInternet();
  if(isNet){
    try {
      let response = await fetch(`${SITENAME}${url}`);
      console.log(response);
      
      let responseJson = await response.json();
      return responseJson.json;
    } catch(error) {
      console.error(error);
    }
  }
}

async function getRawData(url) {
  let isNet = await getInternet();
  if(isNet){
    try {
      let response = await fetch(`${SITENAME}${url}`);
      let responseJson = await response.json();
      return responseJson;
    } catch(error) {
      console.error(error);
    }
  }
}

async function getDataGET(url,param) {
  let isNet = await getInternet();
  if(isNet){
    try {
      let querystring = Utilities.jsonToQueryString(param);
      url = `${url}?${querystring}`;      
      let response = await fetch(`${SITENAME}${url}`);
      let responseJson = await response.json();
      return responseJson;
    } catch(error) {
      console.error(error);
    }
  }
}

async function getGroup() {
  let isNet = await getInternet();
  if(isNet){
    try {
      let response = await fetch(`${SITENAME}ParentCommunities/getGroup.json`);
      let responseJson = await response.json();
      return responseJson.json;
    } catch(error) {
      console.error(error);
    }
  }
}

async function getComm(comm_id) {
  let isNet = await getInternet();
  if(isNet){
    try {
      let response = await fetch(`${SITENAME}Communities/getCommunityByGroup/${comm_id}.json`);
      let responseJson = await response.json();
      return responseJson.json;
    } catch(error) {
      console.error(error);
    }
  }
}

async function getInternet() {
  let response = await NetInfo.isConnected.fetch().then(isConnected => {
    return isConnected;
  });
  return response;
}

export default FetchHtml;
