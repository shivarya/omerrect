import React, { Component } from 'react';
import { NetInfo } from 'react-native';

const config = require('../source/config.json')

const SITENAME = config.sitename;

const PostData = {
  simplePost  : simplePost,
  imagePost   : imagePost
}

async function simplePost(url, postData) {
  let isNet = await getInternet();
  if(isNet){
    try {
      console.log(postData);
      var formData = new FormData();
      for (var k in postData) {
        formData.append(k, postData[k]);
      }
      if (__DEV__) {
        console.log(formData);
      }
      let response = await fetch(`${SITENAME}${url}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },
        body: formData
      })

      let responseJson = await response.json();
      if (__DEV__) {
        console.log(responseJson);
      }
      return responseJson;
    } catch(error) {
      console.error(error);
    }
  }
}

async function imagePost(url, image) {
  let isNet = await getInternet();
  if (isNet) {
    try {
      var formData = new FormData();
      formData.append('uploaded_file', {
        uri: image,
        type: 'image/jpeg', // or photo.type
        name: 'image.jpg'   
      });
      if (__DEV__) {
        console.log(formData);
      }
      let response = await fetch(`${SITENAME}${url}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },
        body: formData
      })

      let responseJson = await response.json();
      if (__DEV__) {
        console.log(responseJson);
      }
      return responseJson.json;
    } catch (error) {
      console.error(error);
    }
  }
}

async function getInternet() {
  let response = await NetInfo.isConnected.fetch().then(isConnected => {
    return isConnected;
  });
  console.log(response);
  return response;
}

export default PostData;
