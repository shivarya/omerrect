import {View, StyleSheet, Text, WebView } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import TopHeader from './TopHeader';
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

class TermNConScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      visible: true,
      datahtml:null
    }
  }

  componentDidMount() {
    FetchHtml.getData('terms.json').then((responseJson) => {
      this.setState({
        visible: false,
        datahtml: responseJson,
      }, function() {
        // do something with new state
      });
    })
    .catch((error) => {
      console.error(error);
    });
  }


  render() {
    return(
      <MenuContext>
        <TopHeader {...this.props} />
        {
          this.state.datahtml &&
          <WebView
            source={{html: this.state.datahtml}}
          />
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
  pageContainer: {
      flex: 1,
      marginTop:0
  }
});

export default TermNConScreen;
