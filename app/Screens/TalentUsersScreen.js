import { View, StyleSheet, Text, TouchableOpacity, ListView, WebView, Image, ToastAndroid } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Card, ListItem, Button, Header } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation'

const config = require('../source/config.json');
const SITENAME = config.sitename;
const Colors = require('../source/colors.json');
import UserSession from '../Components/UserSession' 

class TalentUsersScreen extends Component {
    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            visible: true,
            list: ds.cloneWithRows([]),
            title:'Help',
            community_id : null,
        }
    }

    componentDidMount() {
        UserSession.getCommunity().then(comm => {
            this.setState({title: 'Talents'});
            this.setState({ community_id: comm });
            let url = `Talents/restTalent.json?community_id=${comm}`;
            console.log(url);
            FetchHtml.getRawData(url).then((response) => {
                let responseJson = response.json;
                const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                this.setState({
                    visible: false,
                    list: ds.cloneWithRows(responseJson.data)
                });                
            })
            .catch((error) => {
                console.error(error);
            });
        }).catch ((error) => { 
            console.error(error);
        });
         
    }

    _onPressButton(id) {
        this.props.navigation.navigate('Profile', { profile_id: id,ref:'talent' });
    }

    listRow(u){
        let image = require('../source/img/profile-pic.jpg');
        let gender = u.Profile.gender == "M" ? 'Male' : 'Female';
        if (u.Profile.gender == 'F') {
            image = require('../source/img/fe-avatar.jpg')
        }
        if (u.Detail.ProfilePic && u.Detail.ProfilePic.image) {
            uri = `${SITENAME}files/profile_pic/image/${u.Detail.id}/${u.Detail.ProfilePic.image}`;
            image = { uri: uri };
        }
        return (
            <TouchableOpacity 
                style={Wrapper.boxlist}
            >
                <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={image}
                />
                <View style={styles.profile}>
                    <Text style={styles.heading}>{u.Detail.Name}</Text>
                    <Text style={styles.lbl}>{u.Group.Name}</Text>
                </View>
                <View style={{ width: '100%', flexDirection: 'row'}}> 
                    <TouchableOpacity onPress={() => this._onPressButton(u.Detail.id)} style={styles.button}>
                        <Text style={styles.txt}>Info</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        );    
    }


  render() {
    return(
      <MenuContext>
        <Header
            leftComponent={(<Icon
                name="long-arrow-left"
                size={25}
                color="white"
                onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            />
            )}
            style={{ ...Wrapper.subheader, paddingLeft: 5 }}
            statusBarProps={{ barStyle: 'light-content' }}
            centerComponent={{ text: this.state.title, style: { color: '#fff', fontSize: 23 } }}
        />
        {
            this.state.list &&
            <ListView
                enableEmptySections={true}
                dataSource={this.state.list}
                renderRow={(rowData) => this.listRow(rowData)}
            />
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        marginTop:0
    },
    image: {
        width: '25%',
        height: 70, 
        padding: 1,
        borderWidth: 0.5,
        borderColor: '#ccc'
    },
    profile: {
        width:'65%', 
        marginLeft: 2,
        borderColor: '#DCEBF7'
    },
    heading : {
        fontSize:16,
        paddingLeft:8,  
    },
    lbl: {
        fontSize: 14,
        paddingLeft: 8,
        color:Colors.grey.grey500
    },
    button:{
        width:'30%',
        padding:5,
        backgroundColor:Colors.purple.purple700,
        marginLeft:10,
        marginTop:10 
    },
    txt:{
        color: 'white',
        textAlign:'center'
    }
});

export default TalentUsersScreen;
