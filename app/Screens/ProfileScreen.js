import {View, StyleSheet, Text, WebView, ScrollView,TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Header, Avatar, Button } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import Utilities from '../Components/Utilities';
import UserSession from '../Components/UserSession';

const config = require('../source/config.json');
const SITENAME = config.sitename;

const Colors = require('../source/colors.json');

import Moment from 'moment';

class ProfileScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            profile:null,
            isLogin:false,
            myProfile:false,
            image:null
        }
        this.type = 'main'
    }

    componentDidMount() {
        let state = this.props.navigation.state;
        if (state.params && state.params.profile_id) {
            this.setState({ visible: true });
            let profile_id = state.params.profile_id  
            let url = `Profiles/viewRest/${profile_id}.json`;
            if(state.params.ref){
                url+=`?ref=talent`
            }
            
            FetchHtml.getRawData(url).then((responseJson) => {
                console.log(responseJson);            
                let profile = responseJson.post;
                let u = profile;
                if (u.Detail && u.Detail.ProfilePic.image) {
                    this.setState({ image: u.Detail.ProfilePic.image});
                }
                this.setState({
                    visible: false,
                    profile: profile
                });
            })
            .catch ((error) => {
                console.error(error);
            });
        }
        try {
            UserSession.checkSession().then((is_session) => {
                if (is_session) {
                    this.setState({isLogin: true });
                }
                try {
                    UserSession.getSession().then((users) => {                        
                        if (users && state.params.profile_id == users.profile_id){
                            this.setState({myProfile:true});
                        }                        
                    });
                } catch (error) {
                    console.error('session error: ' + error.message);
                }
            });
        } catch (error) {
            console.error('session error: ' + error.message);
        }
    }

    getProfile(){
        let u = this.state.profile; 
        let image = require('../source/img/profile-pic.jpg');
        let gender = u.Profile.gender == "M" ? 'Male' : 'Female';
        let husband = "Wife";
        if (u.Profile.gender == 'F') {
            image = require('../source/img/fe-avatar.jpg')
            husband = "Husband"
        }
        if (u.Detail && u.Detail.ProfilePic.image) {
            var timeStamp = Math.floor(Date.now() / 1000); 
            uri = `${SITENAME}files/profile_pic/image/${u.Detail.id}/${u.Detail.ProfilePic.image}?dummy=${timeStamp}`;
            image = { uri: uri };
        }
        let address = Utilities.makeAddress(u.Detail.Address);
        let std = "";
        if (u.Detail.Address && u.Detail.Address.City && u.Detail.Address.City.std){
            std = u.Detail.Address.City.std;
        }
        let contact = '';
        if (u.Detail.Contact) {
            contact = Utilities.makeContact(u.Detail.Contact);
        }

        let father = '';
        let fatherLate = false;
        if (u.FatherDetail && u.FatherDetail.Name) {
            if (u.Father.late == 1) fatherLate = true;
            father += u.FatherDetail.Name;
        } else {
            father = u.CommunityGroup.father;
        }

        let mart = "Unmarried";
        if (u.Profile.mrg_status) {
            if (u.Profile.mrg_status == 'Mr') mart = 'Married';
            else if (u.Profile.mrg_status == 'Wi') {
                if (gender == 'Male') mart = 'Widower';
                else mart = 'Widow';
            } else if (u.Profile.mrg_status == 'Di') mart = 'Divorced';
            else mart = 'Unmarried';
        } else mart = 'Unmarried';

        let community = '';
        if (u.CommunityGroup.Community.Name) {
            community = u.CommunityGroup.Community.Name;
        }
        let disability = '';
        if (u.Profile.disability) {
            if (u.Profile.disability == 'Pc') disability = 'Physically challenged';
            else disability = 'Normal';
        }

        let wife = '';
        let wifeLate = false;
        if (u.Profile.mrg_status == 'Mr'){
            if (u.WifeDetail.Name) {
                if (u.LinkedWife.late == 1) wifeLate = true;
                wife += u.WifeDetail.Name
            }else{
                if (u.Wife.late == 1) wifeLate = true;
                wife += u.Wife.Name
            }
        }
        Moment.locale('en');
        let dob = '';
        if(u.Profile.dob){   
            dob = Moment(u.Profile.dob).format('DD-MM-YYYY');
        }
        let occupation = '';
        if (u.Profile.occupation){
            occupation = u.Profile.occupation;
        }

        let ancst_place = '';
        if (ancst_place){
            ancst_place = u.CommunityGroup.ancst_place;
        }
        
        return (
        <View style={styles.pageContainer}>
            <View style={styles.avatar}>
                <Avatar
                    xlarge
                    rounded
                    source={image}
                />
            </View>
            <View style={styles.container}>
                <Text style={styles.headline}>{u.Detail.Name}</Text>
                {this.getRow('Address', address)}
                {this.getRow('Contact', contact)}
                { this.state.isLogin && dob.length > 0 && this.getRow('Date of Birth', dob)}
                { this.state.isLogin && this.getRow('Email', (u.Detail.Contact && u.Detail.Contact.Email) ? u.Detail.Contact.Email : "") }
                
                
                {this.getRow('Gender', gender)}
                {this.getRow('Marital Status', mart)}
                {this.getRow('Physical Status', disability)}
                <View style={styles.rowHeader}>
                    <Text style={[styles.rowBold, styles.purpleBg]}>Father: </Text>
                    <Text style={styles.rowValue}>{fatherLate && this.getLate()}{father}</Text>
                </View>
                {wife.length > 0 && 
                    <View style={styles.rowHeader}>
                        <Text style={[styles.rowBold, styles.purpleBg]}>{husband}: </Text>
                        <Text style={styles.rowValue}>{wifeLate && this.getLate()}{wife}</Text>
                    </View>
                }
                {!!u.Qualification && !!u.Qualification.Name && this.state.isLogin && this.getRow('Qualification', u.Qualification.Name)}
                {!!u.Profession && !!u.Profession.Name && this.state.isLogin && this.getRow('Profession', u.Profession.Name)}
                {occupation.length > 0 && this.state.isLogin && this.getRow('Occupation', occupation)}
                {this.getRow('Community Detail', community)}
                
                {!!u.CommunityGroup && !!u.CommunityGroup.Part && this.state.isLogin &&
                    u.CommunityGroup.Part.map((o, i) => {
                        return (<View key={i} style={styles.rowHeader}>
                            <Text style={[styles.rowBold, styles.purpleBg]}>{o.SubCommunity.Name}: </Text>
                            <Text style={styles.rowValue}>{o.Name}</Text>
                        </View>);
                    })    
                }
                {!!ancst_place.length > 0 && this.state.isLogin && this.getRow('Ancestor Place', ancst_place)} 
                {!!u.Talent && !!u.Talent.about &&
                    <View>
                        <Text style={styles.heading}>About {u.Detail.Name}: </Text>
                        <WebView style={{width:window.width,height:300}} source={{html:u.Talent.about}}/>
                    </View>
                }
                {   u.Profile.mrg_status != 'Mr' && this.state.isLogin && !!u.Marriage.profile_id && this.getMrg(u)}
                {   !!u.Student && this.state.isLogin && !!u.Student.profile_id && this.studentProf(u)}
            </View>
        </View>
        );
    }

    getLate(){
        return (<Text style={{color:Colors.red.red600}}>Late </Text>)
    }
    
    getMrg(u){
        this.type = 'mrg';
        let body_type = 'Average';
        if (u.Marriage.body_type == 'Ath') body_type = 'Athletic';
        else if (u.Marriage.body_type == 'Slim') body_type = 'Slim';
        else if (u.Marriage.body_type == 'Heavy') body_type = 'Heavy';

        let complexion = 'Wheatish';
        if (u.Marriage.complexion == 'VFair') complexion = 'Very Fair';
        else if (u.Marriage.complexion == 'Fair') complexion = 'Fair';
        else if (u.Marriage.complexion == 'WheatB') complexion = 'Wheatish brown';
        else if (u.Marriage.complexion == 'Dark') complexion = 'Dark';

        let vegi = 'Vegetarian';
        if (u.Marriage.vegi == 'Non') vegi = 'Non Vegetarian';
        else if (u.Marriage.vegi == 'Egg') vegi = 'Eggetarian';

        let drinking = 'No';
        if (u.Marriage.drinking == 'Occ') drinking = 'Occasionaly';
        else if (u.Marriage.drinking == 'Yes') drinking = 'Yes';

        let smoking = 'No';
        if (u.Marriage.smoking == 'Occ') smoking = 'Occasionaly';
        else if (u.Marriage.smoking == 'Yes') smoking = 'Yes';

        let btime = '';
        if (u.Marriage.btime && u.Marriage.btime.length > 0){
            let d = new Moment(u.Marriage.btime, 'HH:mm:ss');
            btime = Moment(d).format('hh:mm A');
        }
        return(
            <View>
                <Text style={styles.heading}>Marriage Details:</Text>
                {!!u.Marriage && !!u.Marriage.income && this.getRow('Monthly Income', `Rs. ${u.Marriage.income}`)}
                {!!u.Marriage && !!u.Marriage.wcity && this.getRow('Current Working City', `${u.Marriage.wcity}`)}
                {!!u.Marriage && !!u.Marriage.birth_place && this.getRow('Birth Place', `${u.Marriage.birth_place}`)}
                {!!u.Marriage && this.getRow('Manglik', u.Marriage.manglik ? "Yes" : "No")}
                {btime.length > 0 && this.getRow('Birth Time', btime )}

                <Text style={styles.heading}>Physical Details:</Text>
                {!!u.Marriage && !!u.Marriage.height && this.getRow('Height', `${u.Marriage.height} cm`)}
                {!!u.Marriage && !!u.Marriage.weight && this.getRow('Weight', `${u.Marriage.height} Kg`)}
                {!!body_type && this.getRow('Body Type', `${body_type}`)}
                {!!complexion && this.getRow('Complexion', `${complexion}`)}

                <Text style={styles.heading}>Habits:</Text>
                {!!body_type && this.getRow('Food Type', `${vegi}`)}
                {!!body_type && this.getRow('Drinking', `${drinking}`)}
                {!!body_type && this.getRow('Smoking', `${smoking}`)}
            </View>
        )
    }

    studentProf(u){
        this.type = 'stu';
        return(
            <View>
                <Text style={styles.heading}>Education Details:</Text>
                {!!u.Student && !!u.Student.hobby && this.getRow('Hobbies/Interest', `${u.Student.hobby}`)}
                {!!u.Student && !!u.Student.achievement && this.getRow('Achievements', `${u.Student.achievement}`)}
                {!!u.Student && !!u.Student.des_urself && this.getRow('About', `${u.Student.des_urself}`)}
                {!!u.Student && !!u.Student.skill && this.getRow('Skills', `${u.Student.skill}`)}
                {!!u.Student && !!u.Student.f_income && this.getRow('Parent income', `Rs. ${u.Student.f_income} /Year`)}
            </View>
        )
    }

    getRow(h,v){
        let staray = [];
        let type = this.type;        
        if(type == 'main'){
            staray = [styles.rowBold, styles.purpleBg];
        } else if (type == 'mrg') {
            staray = [styles.rowBold, styles.orangeBg];
        } else if (type == 'about') {
            staray = [styles.rowBold, styles.indigoBg];
        } else if (type == 'stu') {
            staray = [styles.rowBold, styles.greenBg];
        }
        return (
            <View style={styles.rowHeader}>
                <Text style={staray}>{h}: </Text>
                <Text style={styles.rowValue}>{v}</Text>
            </View>
        );
    }

    render() {
        return(
        <MenuContext>
            <Header
                leftComponent={(<Icon
                    name="long-arrow-left"
                    size={25}
                    color="white"
                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                />
                )}
                style={{ ...Wrapper.subheader, paddingLeft: 5 }}
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={{ text: 'Profile View', style: { color: '#fff', fontSize: 23 } }}
            />
            {   this.state.isLogin && this.state.profile && this.state.myProfile &&
                    <View style={styles.btncontainer}>
                        <Button
                            onPress={() => { this.props.navigation.navigate('Edit', { profile_id: this.props.navigation.state.params.profile_id }); }}
                            Component={TouchableOpacity}
                            containerViewStyle={styles.button2}
                            backgroundColor={Colors.blue.blue800}
                            fontSize={13}
                            title='Edit Profile' />
                        <Button
                            onPress={() => { this.props.navigation.navigate('Camera',{image:this.state.image}); }}
                            Component={TouchableOpacity}
                            containerViewStyle={styles.button2}
                            backgroundColor={Colors.blue.blue800}
                            fontSize={13}
                            title='Camera' />
                        <Button
                            onPress={() => { this.props.navigation.navigate('Family',{profile_id:this.props.navigation.state.params.profile_id}); }}
                            Component={TouchableOpacity}
                            containerViewStyle={styles.button2}
                            backgroundColor={Colors.blue.blue800}
                            fontSize={13}
                            title='Family Tree' />                    
                    </View>
            }
            {this.state.profile && <ScrollView style={{marginBottom:10}}>{this.getProfile()}</ScrollView>}
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        </MenuContext>
        );
    }
}

const styles = StyleSheet.create({
    btncontainer: { 
        flexDirection: 'row',
        height:50
    },
    button2: {
        flex:1,
        marginLeft:1,
        marginRight:1
    },
    pageContainer: {
        width:window.width,
        paddingLeft:5,
        paddingRight:5,
        marginTop:10
    },
    avatar:{
        marginTop: 10,
        alignItems: 'center',
        flexDirection: 'column'
    },
    container:{
        width: window.width
    },
    headline: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 0
    },
    rowHeader:{
        flexDirection: 'row',
        borderWidth:0.5,
        borderColor:Colors.grey.grey800
    },
    rowBold:{
        fontWeight:'bold',
        flex:1,
        textAlign:'right',
        padding:5
    },
    rowValue:{
        flex: 2,
        padding: 5,
        backgroundColor: 'white',
        color: 'black'
    },
    purpleBg:{
        backgroundColor:Colors.purple.purple400,
        color:'white' 
    },
    whiteBg: { 
        backgroundColor: 'white',
        color:'black' 
    },
    orangeBg:{
        backgroundColor: Colors.orange.orange400,
        color: 'white' 
    },
    greenBg:{
        backgroundColor: Colors.green.green400,
        color: 'white' 
    },
    indigoBg:{
        backgroundColor: Colors.indigo.indigo600,
        color: 'white' 
    },
    heading: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
        marginLeft:5
    },
    
});

export default ProfileScreen;
