import { View, StyleSheet, Text, TouchableOpacity, ListView, Linking,ToastAndroid } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Card, ListItem, Button, Header } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation'

class CategoryScreen extends Component {
  constructor(props){
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
        visible: true,
        list: ds.cloneWithRows([])
    }
  }

    componentDidMount() {
        FetchHtml.getData('home/getCats.json').then((responseJson) => {
            console.log(responseJson);
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
            this.setState({
                visible: false,
                list: ds.cloneWithRows(responseJson)
            });
        })
        .catch((error) => {
        console.error(error);
        });
    }

    _onPressButton(rowData){
        let Cat = rowData.Cat;
        if (Cat.sub == 1) {
            this.props.navigation.navigate('SubMenu', { id: Cat.id, type:'sub' }); 
        } else {
            if (Cat.type == 'int') {
                console.log(rowData.key);                
                if (rowData.key && rowData.key!= ""){
                    this.props.navigation.navigate('CatView', { model: rowData.key });
                } else if (Cat.link == "HelpTypes/publicv"){
                    this.props.navigation.navigate('HelpView');
                } else if (Cat.link == "Talents/publicv") {
                    this.props.navigation.navigate('TalentUsers');
                } else if (Cat.link == "Home/job") {
                    this.props.navigation.navigate('JobView');
                } else if (Cat.link == "Organizations/publicv") {
                    this.props.navigation.navigate('OrgView');
                }
            } else {
                let link = Cat.link;
                link = 'http:\\'+link;
                Linking.canOpenURL(link).then(supported => {
                    if (supported) {
                        Linking.openURL(link);
                    } else {
                        ToastAndroid.show(`Don't know how to open URI: ${link}`);
                    }
                });
            }
        }
       
    }

    listRow(rowData){
        let Cat = rowData.Cat;
        if(Cat.id == 15){
            return null;
        }else{        
            return (
                <TouchableOpacity 
                    style={Wrapper.catlist}
                    onPress={() => this._onPressButton(rowData)}>
                    <Icon
                        name={Cat.icon}
                        size={20}
                        color="#fff"
                        style={{ flex: 1 }}
                    />
                    <Text style={Wrapper.listtext}>{Cat.Name}</Text>
                    <Icon
                        name='caret-right'
                        size={20}
                        color="#fff"
                        style={{ flex: 1}}
                    />
                </TouchableOpacity>
            );
        }    
    }


    render() {
        return(
        <MenuContext>
            <Header
                leftComponent={(<Icon
                    name="long-arrow-left"
                    size={25}
                    color="white"
                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                />
                )}
                style={{ ...Wrapper.subheader, paddingLeft: 5 }}
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={{ text: 'Categories', style: { color: '#fff', fontSize: 23 } }}
            />
            {
                this.state.list &&
                <ListView
                    enableEmptySections={true}
                    dataSource={this.state.list}
                    renderRow={(rowData) => this.listRow(rowData)}
                />
            }
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        </MenuContext>
        );
    }
}

const styles = StyleSheet.create({
  pageContainer: {
      flex: 1,
      marginTop:0
  },

});

export default CategoryScreen;
