import { View, StyleSheet, Text, TouchableOpacity, ListView, WebView,Image } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Card, ListItem, Button, Header } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation'

const config = require('../source/config.json');
const SITENAME = config.sitename;

import truncate from 'html-truncate'

class CatViewScreen extends Component {
    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            visible: true,
            list: ds.cloneWithRows([]),
            model : null,
            table:null,
            title:'List'
        }
    }

    componentDidMount() {
        let state = this.props.navigation.state;
        if (state.params && state.params.model) {
            let url = `CURD/restList.json?model=${state.params.model}`;
            
            if(state.params.id){
                url += `&relc=${state.params.id}`;
            }
            FetchHtml.getData(url).then((responseJson) => {
                console.log(responseJson);
                const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                this.setState({
                    visible: false,
                    list: ds.cloneWithRows(responseJson.data),
                    model: state.params.model,
                    table: responseJson.model,
                    title: responseJson.title
                });
            })
            .catch((error) => {
                console.error(error);
            });
        }
    }

    _onPressButton(id){
        console.log(id);         
    }

    listRow(rowData){
        let table = rowData[this.state.table];
        console.log(table);
        let model = this.state.model;
        let title = table.Name;
        let id = table.id;
        let image = require('../source/img/no-image.jpg');
        let html = "<p>No Content</p>";
        if(table.descp){
            let html = truncate(table.descp,300);
        }
        
        if (table.image){
            let path = `${SITENAME}/files/${model}/image/${id}/`;
            let uri = `${path}medium_${image}`;
            image = {uri : uri};
        }
        return (
            <TouchableOpacity 
                style={Wrapper.boxlist}
            >
                <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={image}
                />
                <View style={styles.profile}>
                    <Text style={styles.heading}>{title}</Text>
                    <WebView style={styles.webview} scalesPageToFit={true} source={{html:html}} />
                </View>
            </TouchableOpacity>
        );    
    }


  render() {
    return(
      <MenuContext>
        <Header
            leftComponent={(<Icon
                name="long-arrow-left"
                size={25}
                color="white"
                onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            />
            )}
            style={{ ...Wrapper.subheader, paddingLeft: 5 }}
            statusBarProps={{ barStyle: 'light-content' }}
            centerComponent={{ text: this.state.title, style: { color: '#fff', fontSize: 23 } }}
        />
        {
            this.state.list &&
            <ListView
                enableEmptySections={true}
                dataSource={this.state.list}
                renderRow={(rowData) => this.listRow(rowData)}
            />
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        marginTop:0
    },
    image: {
        flex: 2,
        height: 70,
        padding: 1,
        borderWidth: 0.5,
        borderColor: '#ccc'
    },
    profile: {
        flex: 5,
        marginLeft: 2,
        borderColor: '#DCEBF7'
    },
    heading : {
        fontSize:16,
        paddingLeft:8
    },
});

export default CatViewScreen;
