import { View, StyleSheet, Text, TouchableOpacity, ListView, WebView, Image, ToastAndroid } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Card, ListItem, Button, Header } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation'

const config = require('../source/config.json');
const SITENAME = config.sitename;
const Colors = require('../source/colors.json');
import UserSession from '../Components/UserSession' 

class JobViewScreen extends Component {
    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            visible: true,
            list: ds.cloneWithRows([]),
            title:'Help',
            community_id : null,
        }
    }

    componentDidMount() {
        UserSession.getCommunity().then(comm => {
            this.setState({ title: 'Job View', community_id: comm});
            let url = `Jobs/search.json?community_id=${comm}`;
            console.log(url);
            FetchHtml.getRawData(url).then((response) => {
                let responseJson = response.json;
                const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                this.setState({
                    visible: false,
                    list: ds.cloneWithRows(responseJson)
                });                
            })
            .catch((error) => {
                console.error(error);
            });
        }).catch ((error) => { 
            console.error(error);
        });
         
    }

    _onPressButton(id) {
        this.props.navigation.navigate('Profile', { profile_id: id});
    }

    listRow(u){
        return (
            <TouchableOpacity style={Wrapper.boxlist2} >
                <View style={styles.row}><Text style={styles.heading}>{u.FunctionalArea.Name}</Text></View>
                <View style={styles.row}><Text style={styles.exp}>{u.Job.min_experince} to {u.Job.max_experince} Yr. Experince</Text></View>
                <View style={styles.row}>
                    <Text style={styles.exp}><Text style={styles.lbl}>Organization:</Text> {u.Organization.Detail.Name}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.exp}><Text style={styles.lbl}>Location:</Text> {u.Address.City.Name}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.exp}><Text style={styles.lbl}>Minimum Qualification:</Text> {u.Qualification.Name}</Text>
                </View>
                {!!u.Job.about && u.Job.about.length > 0 && <View style={styles.row}>
                    <Text style={styles.exp}>{u.Job.about}</Text>
                </View>
                }
                <View style={styles.row}>
                    <Text style={styles.exp}><Text style={styles.lbl}>Salary (Monthly):</Text> Rs. {u.Job.min_salary}-{u.Job.max_salary}</Text>
                </View>
            </TouchableOpacity>
        );    
    }


  render() {
    return(
      <MenuContext>
        <Header
            leftComponent={(<Icon
                name="long-arrow-left"
                size={25}
                color="white"
                onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            />
            )}
            style={{ ...Wrapper.subheader, paddingLeft: 5 }}
            statusBarProps={{ barStyle: 'light-content' }}
            centerComponent={{ text: this.state.title, style: { color: '#fff', fontSize: 23 } }}
        />
        {
            this.state.list &&
            <ListView
                enableEmptySections={true}
                dataSource={this.state.list}
                renderRow={(rowData) => this.listRow(rowData)}
            />
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
    row:{
        width:'100%'
    },
    heading : {
        fontSize:20,
        color:Colors.blue.blue600,
        fontWeight:'bold'
    },
    lbl:{
        fontWeight: 'bold',
        color:Colors.grey.grey500
    },
    exp: {
        color: 'black',
    },
});

export default JobViewScreen;
