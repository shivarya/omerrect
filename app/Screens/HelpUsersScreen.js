import { View, StyleSheet, Text, TouchableOpacity, ListView, WebView, Image, ToastAndroid } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Card, ListItem, Button, Header } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation'

const config = require('../source/config.json');
const SITENAME = config.sitename;
const Colors = require('../source/colors.json');
import UserSession from '../Components/UserSession' 

class HelpUsersScreen extends Component {
    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            visible: true,
            list: ds.cloneWithRows([]),
            title:'Help',
            community_id : null,
            isStudent : false
        }
    }

    componentDidMount() {
        let state = this.props.navigation.state;
        if (state.params && state.params.id && state.params.title) {
            UserSession.getCommunity().then(comm => {
                this.setState({ title: state.params.title, community_id: comm});
                
                let url = `HelpTypes/restUsers/${state.params.id}.json?community_id=${comm}`;
                if (state.params.id == 4) {                    
                    this.setState({ isStudent: true });
                    url = `HelpTypes/students.json?community_id=${comm}`;
                }
                
                console.log(url);
                FetchHtml.getRawData(url).then((response) => {
                    let responseJson = response.json;
                    if (responseJson.response == 'true' || this.state.isStudent){
                        console.log(responseJson);
                        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                        let list = [];
                        if (this.state.isStudent){
                            list = responseJson
                        }else{
                            list = responseJson.data
                        }
                        this.setState({
                            visible: false,
                            list: ds.cloneWithRows(list)
                        });
                    }else{
                        this.setState({visible: false});
                        ToastAndroid.show(`${responseJson.data}`, ToastAndroid.SHORT);
                    }
                    
                })
                .catch((error) => {
                    console.error(error);
                });
            }).catch ((error) => { 
                console.error(error);
            });
        }   
    }

    _onPressButton(id) {
        this.props.navigation.navigate('Profile', { profile_id: id });
    }

    listRow(u){
        let image = require('../source/img/profile-pic.jpg');
        let gender = u.Profile.gender == "M" ? 'Male' : 'Female';
        if (u.Profile.gender == 'F') {
            image = require('../source/img/fe-avatar.jpg')
        }
        if (u.ProfilePic && u.ProfilePic.image) {
            uri = `${SITENAME}files/profile_pic/image/${u.ProfilePic.id}/${u.ProfilePic.image}`;
            image = { uri: uri };
        }
        let name = '';
        if (this.state.isStudent) {
            name = u.Profile.Detail.Name
        } else {
            name = u.Detail.Name
        }
        let showHelp = false;//disabled for now
        return (
            <TouchableOpacity 
                style={Wrapper.boxlist}
            >
                <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={image}
                />
                <View style={styles.profile}>
                    <Text style={styles.heading}>{name}</Text>
                </View>
                <View style={{ width: '100%', flexDirection: 'row'}}> 
                    {showHelp &&
                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.txt}>Help</Text>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity onPress={() => this._onPressButton(u.Profile.id)} style={styles.button}>
                        <Text style={styles.txt}>Info</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        );    
    }


  render() {
    return(
      <MenuContext>
        <Header
            leftComponent={(<Icon
                name="long-arrow-left"
                size={25}
                color="white"
                onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            />
            )}
            style={{ ...Wrapper.subheader, paddingLeft: 5 }}
            statusBarProps={{ barStyle: 'light-content' }}
            centerComponent={{ text: this.state.title, style: { color: '#fff', fontSize: 23 } }}
        />
        {
            this.state.list &&
            <ListView
                enableEmptySections={true}
                dataSource={this.state.list}
                renderRow={(rowData) => this.listRow(rowData)}
            />
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        marginTop:0
    },
    image: {
        width: '25%',
        height: 70, 
        padding: 1,
        borderWidth: 0.5,
        borderColor: '#ccc'
    },
    profile: {
        width:'65%', 
        marginLeft: 2,
        borderColor: '#DCEBF7'
    },
    heading : {
        fontSize:20,
        paddingLeft:8,  
        marginTop:30
    },
    button:{
        width:'30%',
        padding:5,
        backgroundColor:Colors.purple.purple700,
        marginLeft:10,
        marginTop:10 
    },
    txt:{
        color: 'white',
        textAlign:'center'
    }
});

export default HelpUsersScreen;
