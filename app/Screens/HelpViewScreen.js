import { View, StyleSheet, Text, TouchableOpacity, ListView, WebView,Image } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Card, ListItem, Button, Header } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation'

const config = require('../source/config.json');
const SITENAME = config.sitename;
const Colors = require('../source/colors.json');
import UserSession from '../Components/UserSession' 

class HelpViewScreen extends Component {
    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            visible: true,
            list: ds.cloneWithRows([]),
            title:'Help',
            community_id : null,
        }
    }

    componentDidMount() {
        UserSession.getCommunity().then(comm => {
            this.setState({ community_id: comm });
            let url = `HelpTypes/restPublic.json?community_id=${comm}`;
            console.log(url);
            FetchHtml.getRawData(url).then((responseJson) => {
                console.log(responseJson);
                const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                this.setState({ 
                    visible: false,
                    list: ds.cloneWithRows(responseJson.data)
                });
            })
            .catch((error) => {
                console.error(error);
            });
        }).catch ((error) => { 
            console.error(error);
        });    
    }

    _onPressButton(u){
        this.props.navigation.navigate('HelpUsers', { title: `${u.HelpType.Name} Users List`, id: u.HelpType.id });
    }

    listRow(rowData){
        let help = rowData.HelpType;
        let id = help.id;
        let image = '';
        let html = help.short;
        let showJoin = false;//disable for now
        if(id == 4){
            showJoin = false;
        }
        if (help.icon){
            let uri = `${SITENAME}/files/help_type/icon/${id}/vga_${help.icon}`;
            image = {uri : uri};
        }
        return (
            <TouchableOpacity 
                style={Wrapper.boxlist}
            >
                <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={image}
                />
                <View style={styles.profile}>
                    <Text style={styles.heading}>{help.Name}</Text>
                    <WebView style={styles.webview} scalesPageToFit={true} source={{html:html}} />
                </View>
                <View style={{ width: '100%', flexDirection: 'row'}}> 
                    {showJoin &&
                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.txt}>Join</Text>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity onPress={() => this._onPressButton(rowData)} style={styles.button}>
                        <Text style={styles.txt}>View</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        );    
    }


  render() {
    return(
      <MenuContext>
        <Header
            leftComponent={(<Icon
                name="long-arrow-left"
                size={25}
                color="white"
                onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            />
            )}
            style={{ ...Wrapper.subheader, paddingLeft: 5 }}
            statusBarProps={{ barStyle: 'light-content' }}
            centerComponent={{ text: this.state.title, style: { color: '#fff', fontSize: 23 } }}
        />
        {
            this.state.list &&
            <ListView
                enableEmptySections={true}
                dataSource={this.state.list}
                renderRow={(rowData) => this.listRow(rowData)}
            />
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        marginTop:0
    },
    image: {
        width: '25%',
        height: 70, 
        padding: 1,
        borderWidth: 0.5,
        borderColor: '#ccc'
    },
    profile: {
        width:'65%', 
        marginLeft: 2,
        borderColor: '#DCEBF7'
    },
    heading : {
        fontSize:16,
        paddingLeft:8
    },
    button:{
        width:'30%',
        padding:5,
        backgroundColor:Colors.red.red700,
        marginLeft:10,
        marginTop:10 
    },
    txt:{
        color: 'white',
        textAlign:'center'
    }
});

export default HelpViewScreen;
