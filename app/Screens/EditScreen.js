import { View, StyleSheet, Text, WebView, ScrollView, TouchableOpacity, ToastAndroid, processColor, Dimensions } from 'react-native';
import React, { Component } from 'react';

import { Header, Button, FormLabel, FormInput, FormValidationMessage, CheckBox  } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

import DatePicker from 'react-native-datepicker'

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import Utilities from '../Components/Utilities';
import UserSession from '../Components/UserSession';
import validate from '../Elements/Validation/ValidateWrapper'

const config = require('../source/config.json');
const SITENAME = config.sitename;

const Colors = require('../source/colors.json');

import Moment from 'moment';
import PostData from '../Services/PostData';
import FetchHtml from '../Services/FetchHtml';

let gender = [
    { label: 'Male ', value: "M" },
    { label: 'Female ', value: "F" }
];

let physical = [
    { label: 'Normal ', value: "Nr" },
    { label: 'Physically challenged ', value: "Pc" }
];

let maritial = [
    { label: 'Unmarried ', value: "Un" },
    { label: 'Married ', value: "Mr" },
    { label: 'Widow/Widower ', value: "Wi" },
    { label: 'Divorced ', value: "Di" }
];

let interestMrg = [
    { label: 'No ', value: "2" },
    { label: 'Yes ', value: "1" }
];

class FamilyScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            isLogin:false,
            myProfile:false,
            token:null,
            user_id:null,
            father_id:null,
            name:null,
            checked:true,
            childname:null,
            nameEr:null,
            formvisible:true,
            message:'',
            late:false,
            dob: null,
            gender:null,
            physical:null,
            maritial:null,
            interestMrg:null,
            wife:null,
            wifeEr:null,
            wifeLbl:'Wife',
            wifelate:false,
            address:null,
            addressEr:null,
            area:null,
            mobile:null,
            mobileEr:null,
            landline:null,
            landlineEr:null,
            profile:{},
            profile_id:null
        }
    }

    componentDidMount() {
        try {
            UserSession.checkSession().then((is_session) => {
                if (is_session) {
                    this.setState({isLogin: true });
                }
                try {
                    UserSession.getSession().then((users) => {
                        if(users){
                            this.setState({ token: users.token, user_id: users.user_id });
                            let state = this.props.navigation.state;
                            if (state.params && state.params.profile_id) {
                                if (state.params.profile_id == users.profile_id){
                                    this.setState({ myProfile: true, father_id: state.params.father_id,name:state.params.name});
                                }
                                this.setState({ visible: true });
                                let profile_id = state.params.profile_id
                                let url = `Profiles/viewRest/${profile_id}.json?`;

                                
                                FetchHtml.getRawData(url).then((response) => {
                                    console.log(response);
                                    let profile = response.post;
                                    let genderl = profile.Profile.gender;
                                    console.log(profile);;                 
                                    this.setState({
                                        visible: false,
                                        profile: profile,
                                        childname: !!profile.Detail.Name ? profile.Detail.Name : null,
                                        late: profile.Profile.late == 1 ? true : false,
                                        date: !!profile.Profile.dob ? Moment(profile.Profile.dob).format('DD/MM/YYYY') : null,
                                        gender: genderl,
                                        physical: !!profile.Profile.disability ? profile.Profile.disability : "Nr",
                                        maritial: !!profile.Profile.mrg_status ? profile.Profile.mrg_status : "Un",
                                        interestMrg: !!profile.Profile.mrg_int ? profile.Profile.mrg_int : "2",
                                        wife: !!profile.Wife ? profile.Wife.Name : null,
                                        wifeLbl: genderl == "M" ? 'Wife' : "Husabnd",
                                        wifelate: (!!profile.Wife && !!profile.Wife.late && profile.Wife.late == "1") ? true : false,
                                        address: !!profile.Detail.Address ? profile.Detail.Address.add_line1 : null,
                                        area: !!profile.Detail.Address ? profile.Detail.Address.Area : null,
                                        mobile: !!profile.Detail.Contact ? profile.Detail.Contact.mobile : null,
                                        landline: !!profile.Detail.Contact ? profile.Detail.Contact.landline : null,
                                        profile_id:profile_id
                                    },function(){
                                        this.refs.maritial.updateIsActiveIndex(this.getIndexByValue(maritial,this.state.maritial))
                                        this.refs.gender.updateIsActiveIndex(this.getIndexByValue(gender, this.state.gender))
                                        this.refs.interestMrg.updateIsActiveIndex(this.getIndexByValue(interestMrg, this.state.interestMrg))
                                        this.refs.physical.updateIsActiveIndex(this.getIndexByValue(physical, this.state.physical))
                                        
                                    });
                                   
                                    
                                })
                                .catch((error) => {
                                    console.error(error);
                                });
                            }
                        }                                             
                    });
                } catch (error) {
                    console.error('session error: ' + error.message);
                }
            });
        } catch (error) {
            console.error('session error: ' + error.message);
        }
    }

    boldText(label, required = false) {
        if (required) {
            return (<Text style={{ fontWeight: 'bold', fontSize: 16, marginLeft: 5 }}>{label}: <Text style={{ color: 'red' }}>*</Text></Text>);
        } else {
            return (<Text style={{ fontWeight: 'bold', fontSize: 16, marginLeft: 5 }}>{label}:</Text>);
        }
    }

    add(){
        const nameEr = validate('name', this.state.childname)
        this.setState({
            nameEr: nameEr
        })
        
        if (!nameEr) {
            let state = this.state;
            console.log(this.state);
            let mobile = state.mobile.split(",");
            let post = {
                "data[Profile][gender]": state.gender,
                "data[Detail][Name]": state.childname,
                "data[Profile][late]" : state.late ? 1 : 0,
                "data[Profile][disability]": state.physical,
                "data[Profile][mrg_status]" : state.maritial,
                "data[Profile][mrg_int]" : state.interestMrg,
                "data[Detail][Address][add_line1]" : state.address,
                "data[Detail][Address][Area]" : state.area,
                "data[Detail][Address][city_id]" : 125,
                "data[Detail][Contact][landline]" : state.landline,
                "data[Detail][Contact][mobile1]": mobile[0],
                "data[Profile][dob1]" : state.date
            }

            if (mobile[1]){
                post["data[Detail][Contact][mobile2]"] = mobile[1]
            }

            if(this.state.wife){
                post["data[Wife][id]"] = this.state.profile.Wife.id
                post["data[Wife][Name]"] = this.state.wife;
                post["data[Wife][late]"] = this.state.wifelate ? 1 : 0
            }
            this.setState({
                visible: true
            });
            let url = `Profiles/restEdit/${this.state.profile_id}.json?token=${this.state.token}&user_id=${this.state.user_id}`;
            console.log(post);
            
            PostData.simplePost(url, post).then((res) => {
                console.log(res);
                
                let responseJson = res.json;                    
                this.setState({
                    visible: false
                });
                if (responseJson.response == "true") {
                    this.setState({
                        formvisible: false,
                        message: "Data Updated Succesfully",
                        color: Colors.green.green700
                    });
                } else {
                    var msg = JSON.stringify(responseJson.data);
                    ToastAndroid.show(`${msg}`, ToastAndroid.LONG);
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }
    }

    getIndexByValue(prop,value){
        var i = 0;
        for (var index = 0; index < prop.length; index++) {
            if(prop[index].value == value){
                console.log(value);
                
                i = index;
            }
        }
        return i;
    }

    render() {
        return(
        <MenuContext>
            <Header
                leftComponent={(<Icon
                    name="long-arrow-left"
                    size={25}
                    color="white"
                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                />
                )}
                style={{ ...Wrapper.subheader, paddingLeft: 5 }}
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={{ text: 'Edit Profile', style: { color: '#fff', fontSize: 23 } }}
            />
                {this.state.formvisible &&
                    <ScrollView style={{ height: this.state.visibleHeight, paddingLeft: 15,paddingRight:15, marginBottom: 30 }}>

                        {this.boldText('Name', true)}
                        <FormInput
                            onChangeText={value => this.setState({ childname: value.trim() })}
                            placeholder={'Enter Full Name'}
                            onBlur={() => {
                                this.setState({
                                    nameEr: validate('name', this.state.childname)
                                })
                            }}
                            value = {this.state.childname}
                        />
                        {this.state.nameEr && <FormValidationMessage >{this.state.nameError}</FormValidationMessage>}

                        {this.boldText('Late')} 
                        <CheckBox
                            title='Late'
                            checked={this.state.late}
                            onPress={() => {this.setState({late: !this.state.late})}}
                        />

                        {this.boldText('Gender', true)}
                        <View style={styles.radioStyle}>
                            <RadioForm
                                ref='gender'
                                radio_props={gender}
                                initial={0}
                                formHorizontal={true}
                                onPress={(value) => { if (typeof value !== 'object') {this.setState({ gender: value })}}}
                            />
                        </View>

                        {this.boldText('Physical Status', true)}
                        <View style={styles.radioStyle}>
                            <RadioForm
                                ref='physical'
                                radio_props={physical}
                                initial={0}
                                formHorizontal={true}
                                onPress={(value) => { if (typeof value !== 'object') {this.setState({ physical: value }) }}}
                            />
                        </View>

                        {this.boldText('Marital Status', true)}
                        <View style={styles.radioStyle}>
                            <RadioForm
                                ref='maritial'
                                radio_props={maritial}
                                initial={0}
                                formHorizontal={true}
                                onPress={(value) => { if (typeof value !== 'object') {this.setState({ maritial: value }) }}}
                            />
                        </View>

                        {this.boldText('Interested in Marriage?', true)}
                        <View style={styles.radioStyle}>
                            <RadioForm
                                ref='interestMrg'
                                radio_props={interestMrg}
                                initial={0}
                                formHorizontal={true}
                                onPress={(value) => { if (typeof value !== 'object') {this.setState({ interestMrg: value });}}}
                            />
                        </View>

                        {this.boldText('Date of Birth', true)}
                        <DatePicker
                            style={{ width: '90%',paddingLeft:15 }}
                            date={this.state.date}
                            mode="date"
                            placeholder="Select DOB"
                            format="DD-MM-YYYY"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            onDateChange={(date) => { this.setState({ date: date }) }}
                        />
                        {!!this.state.profile.Wife && !!this.state.profile.Wife.id &&
                            <View style={styles.box}>
                                {this.boldText(`${this.state.wifeLbl} Name`, true)}
                                <FormInput
                                    onChangeText={value => this.setState({ wife: value.trim() })}
                                    placeholder={'Enter Full Name'}
                                    onBlur={() => {
                                        this.setState({
                                            wifeEr: validate('wife', this.state.wife)
                                        })
                                    }}
                                    value={this.state.wife}
                                />
                                {this.state.wifeEr && <FormValidationMessage >{this.state.wifeEr}</FormValidationMessage>}

                                {this.boldText('Late')}
                                <CheckBox
                                    title='Late'
                                    checked={this.state.wifelate}
                                    onPress={() => {
                                        this.setState({
                                            wifelate: !this.state.wifelate
                                        })
                                    }}
                                />
                            </View>
                        }

                        <View style={styles.box}>
                            {this.boldText(`Address`, true)}
                            <FormInput
                                onChangeText={value => this.setState({ address: value.trim() })}
                                placeholder={'Enter Full Address'}
                                onBlur={() => {
                                    this.setState({
                                        addressEr: validate('address', this.state.address)
                                    })
                                }}
                                value={this.state.address}
                            />
                            {this.state.addressEr && <FormValidationMessage >{this.state.addressEr}</FormValidationMessage>}

                            {this.boldText(`Area`, true)}
                            <FormInput
                                onChangeText={value => this.setState({ area: value.trim() })}
                                placeholder={'Enter Area Name'}
                                onBlur={() => {
                                    this.setState({
                                        areaEr: validate('area', this.state.area)
                                    })
                                }}
                                value={this.state.area}
                            />
                            {this.state.areaEr && <FormValidationMessage>{this.state.areaEr}</FormValidationMessage>}

                        </View>

                        <View style={styles.box}>
                            {this.boldText(`Landline No.`, true)}
                            <FormInput
                                onChangeText={value => this.setState({ landline: value.trim() })}
                                placeholder={'Landline No.'}
                                onBlur={() => {
                                    this.setState({
                                        landlineEr: validate('landline', this.state.landline)
                                    })
                                }}
                                value={this.state.landline}
                            />
                            {this.state.landlineEr && <FormValidationMessage >{this.state.landlineEr}</FormValidationMessage>}

                            {this.boldText(`Mobile No.`, true)}
                            <FormInput
                                onChangeText={value => this.setState({ mobile: value.trim() })}
                                placeholder={'Mobile No.'}
                                onBlur={() => {
                                    this.setState({
                                        mobileEr: validate('mobile', this.state.mobile)
                                    })
                                }}
                                value={this.state.mobile}
                            />
                            {this.state.mobileEr && <FormValidationMessage >{this.state.mobileEr}</FormValidationMessage>}

                        </View>
                        <View style={{ flexDirection: 'row',marginTop:10 }}>
                            <TouchableOpacity style={{ width: deviceWidth * 0.5, backgroundColor: Colors.purple.purple500  }}>
                                <Button title='Update' backgroundColor={Colors.purple.purple500} onPress={() => this.add()} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ width: deviceWidth * 0.5, backgroundColor: Colors.red.red500 }}>
                                <Button 
                                    title='Cancel'
                                    backgroundColor= {Colors.red.red500}
                                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                                />
                            </TouchableOpacity>
                        </View>

                    </ScrollView>
                }
                {
                    !this.state.formvisible &&
                    <View visible={!this.state.formvisible} style={{ padding: 5, flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        {<Text style={{ fontSize: 30, color: this.state.color, textAlign: 'center' }}>{this.state.message}</Text>}
                    </View>
                }
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        </MenuContext>
        );
    }
}

const styles = StyleSheet.create({
    maintext:{
        fontSize:16,
        fontWeight:'bold',
        textAlign:'center',
        marginBottom:10,
        marginTop:10,
        textAlign:'center'
    },
    maintext2:{
        color:Colors.teal.teal800,
    },
    rowContainer: {
        flexDirection:'row',
        height:'auto',
    },
    button:{
        flex:1,
        marginLeft:1,
        marginRight:1,
        paddingTop:0
    },
    btnStyle:{
        padding:7
    },
    heading: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: 20,
        flex:1,
        paddingTop:0,
    },
    lbl:{
        fontWeight:'bold',
        color:'black',
        fontSize: 14,
    },
    lblh:{
        width:50,
        fontSize:16,
        color:Colors.grey.grey800
    },
    radioStyle:{
        paddingVertical: 5, 
        paddingLeft: 10
    },
    box: {
        borderWidth: 0.5,
        borderColor: Colors.grey.grey500,
        paddingLeft: 5,
        paddingRight: 5,
        marginTop: 5
    },
    
});
const deviceWidth = Dimensions.get('window').width;

export default FamilyScreen;
