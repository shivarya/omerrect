import {View, StyleSheet, Text, WebView, ScrollView } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Header,Avatar } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import Utilities from '../Components/Utilities';
import UserSession from '../Components/UserSession';

const config = require('../source/config.json');
const SITENAME = config.sitename;

const Colors = require('../source/colors.json');

import Moment from 'moment';

class OrgScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            profile:null
        }
    }

    componentDidMount() {
        let state = this.props.navigation.state;
        if (state.params && state.params.id) {
            this.setState({ visible: true });
            let id = state.params.id  
            let url = `organizations/view/${id}.json`;
            
            FetchHtml.getRawData(url).then((responseJson) => {
                console.log(responseJson);            
                let profile = responseJson.data;
                this.setState({
                    visible: false,
                    profile: profile,
                });
            })
            .catch ((error) => {
                console.error(error);
            });
        }
    }

    getProfile(){
        let u = this.state.profile; 
        let image = require('../source/img/no-image.jpg');
        if (u.Detail.ProfilePic && u.Detail.ProfilePic.image) {
            uri = `${SITENAME}files/profile_pic/image/${u.Detail.id}/${u.Detail.ProfilePic.image}`;
            image = { uri: uri };
        }
        
        let address = Utilities.makeAddress(u.Detail.Address);
        let std = "";
        if (u.Detail.Address && u.Detail.Address.City && u.Detail.Address.City.std){
            std = u.Detail.Address.City.std;
        }
        let contact = '';
        if(u.Detail.Contact){
            contact = Utilities.makeContact(u.Detail.Contact);
        }

        let community = '';
        if (u.Community.Name) {
            community = u.Community.Name;
        }
        
        return (
        <View style={styles.pageContainer}>
            <View style={styles.avatar}>
                <Avatar
                    xlarge
                    rounded
                    source={image}
                />
            </View>
            <View style={styles.container}>
                <Text style={styles.headline}>{u.Detail.Name}</Text>
                {this.getRow('Community', community)}
                {this.getRow('Address', address)}
                {this.getRow('Contact', contact)}
                {this.getRow('Email', (u.Detail.Contact && u.Detail.Contact.Email) ? u.Detail.Contact.Email : "") }
                {this.getRow('About', u.Organization.about)}

                {!!u.Member && this.getMember(u.Member)}
            </View>
        </View>
        );
    }

    getMember(mems){ 
        let rows = mems.map((o, i) => {
            return ( 
                <View style={{marginBottom:10}} key={i}>
                    <View style={styles.rowHeader}>
                        <Text style={[styles.rowBold, styles.purpleBg]}>Name: </Text>
                        <Text style={styles.rowValue}>{o.Detail.Name}</Text>
                    </View>
                    <View style={styles.rowHeader}>
                        <Text style={[styles.rowBold, styles.purpleBg]}>Post: </Text>
                        <Text style={styles.rowValue}>{o.Post.Name}</Text>
                    </View>
                </View>
            );
        }) 
        return (
            <View>
                <Text style={styles.heading}>Members:</Text>
                {rows}
            </View>
        );
          
    }

    getRow(h,v){
        let staray = [];
        let type = this.type;        
        staray = [styles.rowBold, styles.indigoBg];
        return (
            <View style={styles.rowHeader}>
                <Text style={staray}>{h}: </Text>
                <Text style={styles.rowValue}>{v}</Text>
            </View>
        );
    }

    render() {
        return(
        <MenuContext>
            <Header
                leftComponent={(<Icon
                    name="long-arrow-left"
                    size={25}
                    color="white"
                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                />
                )}
                style={{ ...Wrapper.subheader, paddingLeft: 5 }}
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={{ text: 'Organization View', style: { color: '#fff', fontSize: 23 } }}
            />
            {this.state.profile && <ScrollView style={{marginBottom:10}}>{this.getProfile()}</ScrollView>}
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        </MenuContext>
        );
    }
}

const styles = StyleSheet.create({
    pageContainer: {
        width:window.width,
        paddingLeft:5,
        paddingRight:5,
        marginTop:10
    },
    avatar:{
        marginTop: 10,
        alignItems: 'center',
        flexDirection: 'column'
    },
    container:{
        width: window.width
    },
    headline: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 0
    },
    rowHeader:{
        flexDirection: 'row',
        borderWidth:0.5,
        borderColor:Colors.grey.grey800
    },
    rowBold:{
        fontWeight:'bold',
        flex:1,
        textAlign:'right',
        padding:5
    },
    rowValue:{
        flex: 2,
        padding: 5,
        backgroundColor: 'white',
        color: 'black'
    },
    purpleBg:{
        backgroundColor:Colors.purple.purple400,
        color:'white' 
    },
    whiteBg: { 
        backgroundColor: 'white',
        color:'black' 
    },
    orangeBg:{
        backgroundColor: Colors.orange.orange400,
        color: 'white' 
    },
    greenBg:{
        backgroundColor: Colors.green.green400,
        color: 'white' 
    },
    indigoBg:{
        backgroundColor: Colors.indigo.indigo600,
        color: 'white' 
    },
    heading: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
        marginLeft:5
    },
    
});

export default OrgScreen;
