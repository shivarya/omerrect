import {View,StyleSheet,Text} from 'react-native';
import React, { Component } from 'react';
import { Avatar} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { DrawerNavigator,DrawerItems } from 'react-navigation';

const config = require('../source/config.json');
const SITENAME = config.sitename;

import UserSession from '../Components/UserSession';

class SideMenu extends Component {
  constructor(props){
    super(props);
    this.state = {
      name: "Guest",
      image: "img/profile-pic.jpg"
    }
  }

  componentDidMount () {
    console.log('componentDidMount');
    try {
      UserSession.getSession().then(userdata => {
        if(userdata != null){
          this.setState({name:userdata.data.Detail.Name})
          let name = userdata.data.Detail.Name
          if('ProfilePic' in userdata.data){
            let imageuri = `files/profile_pic/image/${userdata.id}/${userdata.data.ProfilePic.image}`;
            this.setState({image:imageuri})
          }
        }
      });
    }catch (error) {
      console.error('session error: ' + error.message);
    }
  }

  render() {
    const styles2 = StyleSheet.create({
      headline : {
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 0
      },
      image : {
        height: 80,
        marginTop:10,
        alignItems: 'center',
        flexDirection: 'column'
      }
    });
    // Other logic and such
    return (
      <View>
        <View style={styles2.image}>
          <Avatar
            large
            rounded
            source={{uri: `${SITENAME}${this.state.image}`}}
            title="Profile Pic"
            onPress={() => console.log("Works!")}
            activeOpacity={0.7}
          />
        </View>
        <Text style={styles2.headline}>{this.state.name}</Text>
        <DrawerItems {...this.props}/>
      </View>
    )
  }
}

export default SideMenu;
