import { View, StyleSheet, Text, TouchableOpacity, ListView } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Card, ListItem, Button, Header } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation'

class SubMenuScreen extends Component {
    constructor(props){
        super(props);
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            visible: true,
            list: ds.cloneWithRows([]),
            type:null,
            header: 'Sub-Categories'
        }
    }

    componentDidMount() {
        let state = this.props.navigation.state;
        console.log(state);
        
        if (state.params && state.params.id && state.params.type) {
            let url = null;
            let type = null;
            if (state.params.type == "secsub"){
                url = `CURD/secsubmenu/${state.params.id}.json`;
                type = 'secsub'; 
            }else{
                url = `CURD/submenu/${state.params.id}.json`;
                type = "sub";
            }
            this.setState({ type: type});
            console.log(url);
            
            FetchHtml.getRawData(url).then((response) => {
                let responseJson = response.json;
                let head = response.head;
                let header = "";
                if(this.state.type == 'sub'){
                    header = head.Cat.Name;
                }else{
                    header = head.SubCat.Name;
                }
                const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                this.setState({
                    visible: false,
                    list: ds.cloneWithRows(responseJson),
                    header:header
                });
            })
            .catch((error) => {
                console.error(error);
            });
        }
    }

    _onPressButton(rowData){
        if (this.state.type == "sub") {
            if (rowData.Cat.id == 3){
                this.props.navigation.navigate('CatView', { model: 'biz_listing', id: rowData.SubCat.id });
            }else{
                this.props.navigation.navigate('SubMenu', { id: rowData.SubCat.id, type: 'secsub' })
            }            
        }else{
            this.props.navigation.navigate('CatView', { model: 'rel_desc', id: rowData.SecSubCat.id });
        }
    }

    listRow(rowData){
        let name = "";
        if(this.state.type == "sub"){
            name = rowData.SubCat.Name;
        }else{
            name = rowData.SecSubCat.Name;
        }
        return (
            <TouchableOpacity 
                style={Wrapper.subcatlist}
                onPress={() => this._onPressButton(rowData)}>
                <Text style={Wrapper.listtext}>{name}</Text>
                <Icon
                    name='caret-right'
                    size={20}
                    color="#fff"
                    style={{ flex: 1}}
                />
            </TouchableOpacity>
        );    
    }


  render() {
    return(
      <MenuContext>
        <Header
            leftComponent={(<Icon
                name="long-arrow-left"
                size={25}
                color="white"
                onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            />
            )}
            style={{ ...Wrapper.subheader, paddingLeft: 5 }}
            statusBarProps={{ barStyle: 'light-content' }}
            centerComponent={{ text: this.state.header, style: { color: '#fff', fontSize: 23 } }}
        />
        {
            this.state.list &&
            <ListView
                enableEmptySections={true}
                dataSource={this.state.list}
                renderRow={(rowData) => this.listRow(rowData)}
            />
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
  pageContainer: {
      flex: 1,
      marginTop:0
  },

});

export default SubMenuScreen;
