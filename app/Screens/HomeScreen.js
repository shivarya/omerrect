import { View, StyleSheet, Text, Picker, Dimensions, ToastAndroid, AsyncStorage, NetInfo, TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';
import { Button} from 'react-native-elements';

import TopHeader from './TopHeader';
import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const Colors = require('../source/colors.json');

import { StackNavigator } from 'react-navigation';

class HomeScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      groups: [{
        ParentCommunity:{
          id:0,
          Name:'Select Group'
        }
      }],
      isConnected:false,
      selectedGroup:0,
      comms: [defaultComm],
      selectedComms:0,
      visible: true,
      buttons:[
        { icon: 'magnify', title: 'Search By Name', screen: 'Search', color: Colors.blue.blue600},
        { icon: 'ring', title: 'Matrimony', screen: 'Search', params: { is_mrg: 1 }, color: Colors.blue.blue600},        
        { icon: 'format-align-justify', title: 'Categories', screen: 'Category', color: Colors.blue.blue600},
        { icon: 'newspaper', title: 'Latest Updates', screen: 'News', color: Colors.blue.blue600},
      ]
    }
  };

  componentWillMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected){
        this.setState({isConnected:true});
        FetchHtml.getGroup().then((responseJson) => {
          this.state.groups.push.apply(this.state.groups,responseJson);
          this.setState({
            visible: false,
            groups: this.state.groups
          }, function() {

          });
        })
        .catch((error) => {
          console.error(error);
        });
      }else{
        this.setState({visible:false});
      }
    });
    AsyncStorage.getItem('group').then((group) => {
      if(group != null){
        this.setState({selectedGroup: group});
        this.getComm(group);
      }
    });
    if(this.state.selectedGroup!=0 && this.state.selectedComms == 0){
      let msg = "Select Community";
      ToastAndroid.show(`${msg}`, ToastAndroid.SHORT);
    }
    let state = this.props.navigation.state;
    if(state.params && state.params.login){
      delete this.props.navigation.state.params.login
      ToastAndroid.show(`Login Successfull`, ToastAndroid.SHORT);
    }
    if(state.params && state.params.logout){
      delete this.props.navigation.state.params.logout
      ToastAndroid.show(`Logout Successfull`, ToastAndroid.SHORT);
    }
  };

  async getComm(v,i){
    if(v!=0){
      this.setState({
        visible:true,
        selectedGroup:v
      });
      this.saveItem('group',v);
      await FetchHtml.getComm(v).then((responseJson) => {
        this.state.comms = [defaultComm];
        this.state.comms.push.apply(this.state.comms,responseJson);
        this.setState({
          visible: false,
          comms: this.state.comms,
        }, function() {

        });
      })
      .catch((error) => {
        console.error(error);
      });
      AsyncStorage.getItem('comm').then((comm) => {
        if(comm != null){
          let c = parseInt(comm);
          this.setState({selectedComms: c});
        }
      });
    }else{
      ToastAndroid.show('Select Group to Start', ToastAndroid.SHORT);
    }

  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, `${selectedValue}`);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  setGroup(v,i){
    if(v!=0){
      this.setState({
        selectedComms:v
      });
      this.saveItem('comm',v);
      let comm = this.state.comms[i].Name;
      let msg = `Community Selected ${comm}`;
      ToastAndroid.show(`${msg}`, ToastAndroid.SHORT);
    }
  }

  _onPressButton(s){
    AsyncStorage.getItem('group').then((group) => {
      if(group!=null && group!=0){
        AsyncStorage.getItem('comm').then((comm) => {
          if(comm!=null && comm!=0){
            let screen = s.screen;
            if(s.params){
              this.props.navigation.navigate(screen,s.params);
            }else{
              this.props.navigation.navigate(screen);
            }
            
          }else{
            ToastAndroid.show(`Select Community First`, ToastAndroid.SHORT);
          }
        });
      }else{
        ToastAndroid.show(`Select Group First`, ToastAndroid.SHORT);
      }
    });
  }

  render(){
    let width = Dimensions.get('window').width;
    let fifyPercent = width / 2;
    let groupItems = this.state.groups.map((s, i) => {
        return <Picker.Item key={i} value={s.ParentCommunity.id} label={s.ParentCommunity.Name} />
    });

    let commItems = this.state.comms.map((s, i) => {
        return <Picker.Item key={i} value={s.id} label={s.Name} />
    });

    let buttonsComp = this.state.buttons.map((s, i) => {
        return (
        <TouchableOpacity
          style={[styles.button,{backgroundColor:s.color}]}
          key={i}
          onPress={() => this._onPressButton(s)}>
          <Icon
            name={s.icon}
            size={30}
            color="#fff"
          />
          <Text style={{color:'white',marginTop:10}}>{s.title}</Text>
        </TouchableOpacity>);
    });

    return(
      <MenuContext style={Wrapper.menu}>
        <TopHeader {...this.props} />
        <View style={{flexDirection:'row'}}>
          <Picker
            style={{width:fifyPercent}}
            selectedValue={this.state.selectedGroup}
            onValueChange={(itemValue, itemIndex) => this.getComm(itemValue, itemIndex)}>
            {groupItems}
          </Picker>
          <Picker
            style={{width:fifyPercent}}
            selectedValue={this.state.selectedComms}
            onValueChange={(itemValue, itemIndex) => this.setGroup(itemValue, itemIndex)}>
            {commItems}
          </Picker>
        </View>
        <View style={styles.view}>
          {buttonsComp}
        </View>
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const defaultComm = {
    id:0,
    Name:'Select Community'
};

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  view: {
    flexDirection: 'row',
    justifyContent: 'flex-start', // to adjust the spacing
    flexWrap : 'wrap'
  },
  button: {
    paddingVertical: 30, // 30px padding from top and bottom makes it vertically centered
    margin:deviceWidth * 0.04,
    height: 120,
    width: deviceWidth * 0.41,
    borderRadius:8,
    justifyContent:'center',
    alignItems:'center',
  }
});
export default HomeScreen;
