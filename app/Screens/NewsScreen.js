import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    ListView,
    Linking,
    ToastAndroid,
    Dimensions,
    WebView
} from 'react-native';
import React, {
    Component
} from 'react';
import FetchHtml from '../Services/FetchHtml';

import {
    Card,
    ListItem,
    Button,
    Header
} from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';

import {
    MenuContext
} from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import Icon from 'react-native-vector-icons/FontAwesome';
import {
    NavigationActions
} from 'react-navigation'
import Carousel, {
    ParallaxImage
} from 'react-native-snap-carousel';

const {
    width: viewportWidth,
    height: viewportHeight
} = Dimensions.get('window');

const config = require('../source/config.json');
const SITENAME = config.sitename;
const Colors = require('../source/colors.json');
import UserSession from '../Components/UserSession'

class NewsScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            visible: true,
            community_id:null,
            data:[]
        }
    }

    componentDidMount() {
        UserSession.getCommunity().then(comm => {
            this.setState({
                community_id: comm
            });
            let url = `updates/getUpdate/${comm}.json`;
            //console.log(url);
            FetchHtml.getRawData(url).then((responseJson) => {
                    // console.log(responseJson);
                    const ds = new ListView.DataSource({
                        rowHasChanged: (r1, r2) => r1 !== r2
                    });
                    this.setState({
                        visible: false,
                        data: responseJson.json
                    });
                })
                .catch((error) => {
                    console.error(error);
                });
        }).catch((error) => {
            console.error(error);
        });
    }

    _renderItem({ item, index }, parallaxProps) {
        let Update = item.Update;        
        let id = Update.id;
        let image = `${SITENAME}/files/update/image/${id}/medium_${Update.image}`
        return (
            <View style={{ height: viewportHeight}}> 
                <ParallaxImage
                    source={{ uri: image }}
                    containerStyle={styles.imageContainer}
                    dimensions={{width:viewportWidth/1.8,height:viewportHeight/3.5}}
                    style={styles.image}
                    parallaxFactor={0.4}
                    {...parallaxProps}
                />
                <Text style={{ fontSize: 20, textAlign:'center' }}>{Update.title}</Text>
                <WebView source={{html:Update.content}}  />
            </View>
        );
    }

    render() {
        return(
        <MenuContext>
            <Header
                leftComponent={(<Icon
                    name="long-arrow-left"
                    size={25}
                    color="white"
                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                />
                )}
                style={{ ...Wrapper.subheader, paddingLeft: 5 }}
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={{ text: 'Latest Updates', style: { color: '#fff', fontSize: 23 } }}
            />
            {
                <Carousel
                    data={this.state.data}
                    renderItem={this._renderItem}
                    sliderWidth={viewportWidth}
                    itemWidth={viewportWidth}
                    slideStyle={{ width: viewportWidth }}
                    inactiveSlideOpacity={1}
                    inactiveSlideScale={1}
                    autoplay={true}
                    loop={true} 
                    hasParallaxImages={true}
                />
            }
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        </MenuContext>
        );
    }
}

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        marginTop: 0
    },
    imageContainer: {
        height: viewportHeight / 3.4
    },
    image: {
        padding: 5
    }

});

export default NewsScreen;
