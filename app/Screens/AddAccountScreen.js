import { View, StyleSheet, Text, WebView, ScrollView, TouchableOpacity, ToastAndroid } from 'react-native';
import React, { Component } from 'react';

import { Header, Button, FormLabel, FormInput, FormValidationMessage, CheckBox  } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import Utilities from '../Components/Utilities';
import UserSession from '../Components/UserSession';
import validate from '../Elements/Validation/ValidateWrapper'

const config = require('../source/config.json');
const SITENAME = config.sitename;

const Colors = require('../source/colors.json');

import Moment from 'moment';
import PostData from '../Services/PostData';

class AddAccountScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            isLogin:false,
            myProfile:false,
            token:null,
            user_id:null,
            father_id:null,
            name:null,
            checked:true,
            username: null,
            usernameEr: null,
            password: null,
            passwordEr: null,
            repassword: null,
            repasswordEr: null,
            email: null,
            emailEr: null,
            formvisible:true,
            message:''
        }
    }

    componentDidMount() {
        try {
            UserSession.checkSession().then((is_session) => {
                if (is_session) {
                    this.setState({isLogin: true });
                }
                try {
                    UserSession.getSession().then((users) => {
                        if(users){
                            this.setState({ token: users.token, user_id: users.user_id });
                            let state = this.props.navigation.state;
                            if (state.params && state.params.profile_id) {
                                if (state.params.profile_id == users.profile_id){
                                    this.setState({ myProfile: true, father_id: state.params.father_id,name:state.params.name});
                                }
                            }
                        }                                             
                    });
                } catch (error) {
                    console.error('session error: ' + error.message);
                }
            });
        } catch (error) {
            console.error('session error: ' + error.message);
        }
    }

    add(){
        let emailEr = validate('email', this.state.email)
        let usernameEr = validate('username', this.state.username)
        let passwordEr = validate('password', this.state.password)
        let repasswordEr = validate('repassword', this.state.repassword, this.state.password)

        this.setState({
            emailEr: emailEr,
            usernameEr: usernameEr,
            passwordEr: passwordEr,
            repasswordEr: repasswordEr
        })
        
        if (!emailEr & !usernameEr & !passwordEr & !repasswordEr) {
            let state = this.state;
            let post = {
                "email": state.email,
                "username": state.username,
                "Password": state.password,
                "rePassword": state.repassword,
            }
            this.setState({
                visible: true
            });
            let url = `Users/restAddAccount/${state.father_id}.json?token=${this.state.token}&user_id=${this.state.user_id}`;
            PostData.simplePost(url, post).then((res) => {
                let responseJson = res.json;                    
                this.setState({
                    visible: false
                });
                if (responseJson.response == "true") {
                    this.setState({
                        formvisible: false,
                        message: "User Added Succesfully",
                        color: Colors.green.green700
                    });
                    this.props.navigation.navigate('Family', { profile_id: this.props.navigation.state.params.profile_id }); 
                } else {
                    var msg = JSON.stringify(responseJson.data);
                    ToastAndroid.show(`${msg}`, ToastAndroid.LONG);
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }
    }

    render() {
        return(
        <MenuContext>
            <Header
                leftComponent={(<Icon
                    name="long-arrow-left"
                    size={25}
                    color="white"
                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                />
                )}
                style={{ ...Wrapper.subheader, paddingLeft: 5 }}
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={{ text: 'Add Child', style: { color: '#fff', fontSize: 23 } }}
            />
            {this.state.formvisible && <ScrollView>
                <Text style={styles.maintext}>Add Account for <Text style={{color:Colors.blue.blue700}}>{this.state.name}</Text></Text>
                <FormLabel labelStyle={styles.lbl}>Username</FormLabel>
                <FormInput
                    onChangeText={value => this.setState({ username: value.trim() })}
                    placeholder={'Enter username'}
                    onBlur={() => {
                        this.setState({
                            usernameEr: validate('username', this.state.username)
                        })
                    }}
                />
                {this.state.usernameEr && <FormValidationMessage >{this.state.usernameEr}</FormValidationMessage>}

                <FormLabel labelStyle={styles.lbl}>Email</FormLabel>
                <FormInput
                    onChangeText={value => this.setState({ email: value.trim() })}
                    placeholder={'Enter Email'}
                    onBlur={() => {
                        this.setState({
                            emailEr: validate('email', this.state.email)
                        })
                    }}
                />
                {this.state.emailEr && <FormValidationMessage >{this.state.emailEr}</FormValidationMessage>}

                <FormLabel labelStyle={styles.lbl}>Password</FormLabel>
                <FormInput
                    onChangeText={value => this.setState({ password: value.trim() })}
                    secureTextEntry={true}
                    placeholder={'Enter Password'}
                    onBlur={() => {
                        this.setState({
                            passwordEr: validate('password', this.state.password)
                        })
                    }}
                />
                {this.state.passwordEr && <FormValidationMessage >{this.state.passwordEr}</FormValidationMessage>}

                <FormLabel labelStyle={styles.lbl}>Retype-Password</FormLabel>
                <FormInput
                    onChangeText={value => this.setState({ repassword: value.trim() })}
                    secureTextEntry={true}
                    placeholder={'Retype Password'}
                    onBlur={() => {
                        this.setState({
                            repasswordEr: validate('repassword', this.state.repassword)
                        })
                    }}
                />
                {this.state.repasswordEr && <FormValidationMessage >{this.state.repasswordEr}</FormValidationMessage>}

                <Button title='Submit' 
                    onPress={() => {
                        this.add()
                    }} 
                    backgroundColor={Colors.indigo.indigo600} 
                />
                </ScrollView>
            }
            {
                !this.state.formvisible &&
                <View style={{ padding: 5, flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                    {<Text style={{ fontSize: 30, color: this.state.color, textAlign: 'center' }}>{this.state.message}</Text>}
                </View>
            }
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        </MenuContext>
        );
    }
}

const styles = StyleSheet.create({
    maintext:{
        fontSize:16,
        fontWeight:'bold',
        textAlign:'center',
        marginBottom:10,
        marginTop:10,
        textAlign:'center'
    },
    maintext2:{
        color:Colors.teal.teal800,
    },
    rowContainer: {
        flexDirection:'row',
        height:'auto',
    },
    button:{
        flex:1,
        marginLeft:1,
        marginRight:1,
        paddingTop:0
    },
    btnStyle:{
        padding:7
    },
    heading: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: 20,
        flex:1,
        paddingTop:0,
    },
    lbl:{
        fontWeight:'bold',
        color:'black',
        fontSize: 14,
    },
    lblh:{
        width:50,
        fontSize:16,
        color:Colors.grey.grey800
    },
    
});

export default AddAccountScreen;
