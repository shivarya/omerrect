import {View, StyleSheet, Text, WebView } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import TopHeader from './TopHeader';
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

class ContactScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      visible: false,
      datahtml: this.getHtml()
    }
  }

  // componentDidMount() {
  //   FetchHtml.getData().then((responseJson) => {
  //     this.setState({
  //       visible: false,
  //       datahtml: responseJson,
  //     }, function() {
  //       // do something with new state
  //     });
  //   })
  //   .catch((error) => {
  //     console.error(error);
  //   });
  // }

  getHtml(){
    return (
      '<div> \
        <b>Eamil ID:</b> contact@omersamaj.com<br> \
        <b>Number:</b> (+91) 9555555555<br> \
        <b>Address:</b> Kanpur'
    );
  }


  render() {
    return(
      <MenuContext>
        <TopHeader {...this.props} />
        <WebView
          source={{html: this.state.datahtml}}
        />
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
  pageContainer: {
      flex: 1,
      marginTop:0
  }
});

export default ContactScreen;
