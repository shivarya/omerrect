import { View, StyleSheet, Text, Dimensions, TouchableHighlight, TouchableOpacity, ToastAndroid } from 'react-native';
import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';
import ImagePicker from 'react-native-image-picker';
import { Header, Button,Avatar } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
const Colors = require('../source/colors.json');

const config = require('../source/config.json');
const SITENAME = config.sitename;
import { NavigationActions } from 'react-navigation'
import UserSession from '../Components/UserSession'

import PostData from '../Services/PostData'

let options = {
  title: 'Select Avatar',
  customButtons: [
    { name: 'fb', title: 'Choose Photo from Facebook' },
  ],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  }
};

class CameraScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      visible: false,
      image: `${SITENAME}img/fe-avatar.jpg`,
      saveBtn: false,
      token:null,
      oldimage:null,
      user_id:null
    }
  }

  componentDidMount() {
    try {
      UserSession.checkSession().then((is_session) => {
        if (is_session) {
          this.setState({ isLogin: true });
        }
        try {
          UserSession.getSession().then((users) => {
            this.setState({ token: users.token,user_id:users.user_id });
            let state = this.props.navigation.state;
            if (users.data && users.data.ProfilePic && users.data.ProfilePic.image && state.params && state.params.image){
              let u = users.data;
              var timeStamp = Math.floor(Date.now() / 1000); 
              let uri = `${SITENAME}files/profile_pic/image/${u.ProfilePic.id}/${u.ProfilePic.image}?dummy=${timeStamp}`;            
              this.setState({ image: uri});
            }
          });
        } catch (error) {
          console.error('session error: ' + error.message);
        }
      });
    } catch (error) {
      console.error('session error: ' + error.message);
    }
  }

  takePics(){
    // Launch Camera:
    this.setState({ visible: true })
    ImagePicker.launchCamera(options, (response) => {
      if(!response.didCancel){
        this.setState({ oldimage:this.state.image, image: response.uri, saveBtn: true});
      }
      this.setState({visible: false });           
    });
  }

  choosePics() {
    // Launch Folder:
    this.setState({ visible:true})
    ImagePicker.launchImageLibrary(options, (response) => {
      if (!response.didCancel){
        this.setState({ oldimage: this.state.image,image: response.uri, saveBtn: true});   
      }
      this.setState({visible:false});   
    });
  }

  save(){
    this.setState({ visible: true });
    let url = `ProfilePics/upload?token=${this.state.token}&user_id=${this.state.user_id}`
    PostData.imagePost(url,this.state.image).then((response) => {
      if(response.expire && response.expire == "true"){
          UserSession.removeSession();
          this.props.navigation.navigate('LoginScreen',{expire:true})
      }
      if(response.response == "false"){
        this.setState({image:this.state.oldimage})
      }
      let msg = response.data;
      ToastAndroid.show(`${msg}`, ToastAndroid.LONG);
      this.setState({
        visible: false,
      });
    })
  }


  render() {
    return (
      <MenuContext>
        <Header
          leftComponent={(<Icon
            name="long-arrow-left"
            size={25}
            color="white"
            onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
          />
          )}
          style={{ ...Wrapper.subheader, paddingLeft: 5 }}
          statusBarProps={{ barStyle: 'light-content' }}
          centerComponent={{ text: 'Profile Picture', style: { color: '#fff', fontSize: 23 } }}
        />
        <View style={styles.avatar}>
          <Avatar
            width={250}
            height={250}
            rounded
            source={{uri:this.state.image}}
          />
        </View>
        <Button
          onPress={() => { this.takePics() }}
          Component={TouchableOpacity}
          style={styles.button}
          large
          backgroundColor={Colors.indigo.indigo500} 
          icon={{ name: 'camera', type: 'font-awesome' }}
          title='Take Picture' />
        <Button
          onPress={() => { this.choosePics() }} 
          Component={TouchableOpacity}
          style={styles.button}
          large
          backgroundColor={Colors.indigo.indigo500}
          icon={{ name: 'upload', type: 'font-awesome' }}
          title='Choose from Gallery' />
          { this.state.saveBtn && 
            <View style={styles.container}>
              <Button
                onPress={() => { this.save() }}
                Component={TouchableOpacity}
                containerViewStyle={styles.button2} 
                backgroundColor={Colors.green.green800}
                icon={{ name: 'floppy-o', type: 'font-awesome' }}
                title='Save' />
              <Button
                onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                Component={TouchableOpacity}
                containerViewStyle={styles.button2}
                backgroundColor={Colors.red.red800}
                icon={{ name: 'ban', type: 'font-awesome' }}
                title='Cancel' />
            </View>
          }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row', 
      width:'100%'
    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    capture: {
      flex: 0,
      color: '#000',
      padding: 10,
      margin: 40
    },
    button:{
      marginTop:10
    },
    button2: {
      marginTop: 10,
      flex:1
    },
    avatar: {
      marginTop: 10,
      alignItems: 'center',
      flexDirection: 'column'
    },
  });

export default CameraScreen;
