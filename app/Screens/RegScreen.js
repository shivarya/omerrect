import React, {Component} from 'react'
import {View, Button, Text, Dimensions, Keyboard, ScrollView, TouchableOpacity, NetInfo, Picker} from 'react-native'

import TextField from '../Elements/FormElements/TextField'
import validate from '../Elements/Validation/ValidateWrapper'

import TopHeader from './TopHeader';
import { Header} from 'react-native-elements';

import Wrapper from '../styles/Wrapper';

import { MenuContext } from 'react-native-popup-menu';

const Colors  = require('../source/colors.json');

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import PostData from '../Services/PostData';

import Spinner from 'react-native-loading-spinner-overlay';

import FetchHtml from '../Services/FetchHtml';

import UserSession from '../Components/UserSession';

let radio_props = [
  {label: 'Male ', value: "M" },
  {label: 'Female ', value: "F" }
];

let initial_state = {
  visibleHeight: Dimensions.get('window').height,
  isConnected:false,
  name: null,
  nameEr: '',
  gender: 'M',
  gendertEr: '',
  email:null,
  emailEr:'',
  ancestor:null,
  ancestorEr:'',
  fname:null,
  fnameEr:'',
  father_con:null,
  father_conEr:'',
  CommErr:null,
  username:null,
  usernameEr:'',
  password:null,
  passwordEr:'',
  repassword:null,
  repasswordEr:'',
  error:false,
  visible: false,
  formvisible:true,
  community: [{
    Community:{
      id:0,
      Name:'Select Community'
    }
  }],
  selectedComm:0,
  selectedGotra: [
    0,0,0,0
  ],
  gotra : null
};

class RegScreen extends Component {
  constructor(props) {
    super(props)
    this.state = initial_state;
  }

  componentWillMount () {
    Keyboard.addListener('keyboardWillShow', this.keyboardWillShow.bind(this))
    Keyboard.addListener('keyboardWillHide', this.keyboardWillHide.bind(this))
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected){
        this.setState({isConnected:true});
        FetchHtml.getRawData('Communities/restCommunity.json').then((res) => {
          let responseJson = res.data;
          this.state.community.push.apply(this.state.community,responseJson);
          this.setState({
            visible: false,
            community: this.state.community
          }, function() {

          });
        })
        .catch((error) => {
          console.error(error);
        });
      }else{
        this.setState({visible:false});
      }
    });
  }

  keyboardWillShow (e) {
    let newSize = Dimensions.get('window').height - e.endCoordinates.height
    this.setState({visibleHeight: newSize})
  }

  keyboardWillHide (e) {
    this.setState({visibleHeight: Dimensions.get('window').height})
  }

  register() {
    let nameEr = validate('name', this.state.name)
    let emailEr = validate('email', this.state.email)
    let ancestorEr = validate('ancestor', this.state.ancestor)
    let fnameEr = validate('name', this.state.fname)
    let father_conEr = validate('contact', this.state.father_con)
    let CommErr = validate('community', this.state.selectedComm)
    let usernameEr = validate('username', this.state.username)
    let passwordEr = validate('password', this.state.password)
    let repasswordEr = validate('repassword', this.state.repassword,this.state.password)

    this.setState({
      nameEr: nameEr,
      emailEr: emailEr,
      ancestorEr: ancestorEr,
      fnameEr: fnameEr,
      father_conEr: father_conEr,
      CommErr: CommErr,
      usernameEr: usernameEr,
      passwordEr: passwordEr,
      repasswordEr: repasswordEr
    })
    
    if (!nameEr & !emailEr & !ancestorEr & !fnameEr & !father_conEr & !CommErr & !usernameEr & !passwordEr & !repasswordEr) {
      let state = this.state;
      let post = {
        "name"        : state.name,
        "gender"      : state.gender,
        "email"       : state.email,
        "ancst_place" : state.ancestor,
        "father"      : state.fname,
        "data[Detail][Profile][CommunityGroup][c_father]":  state.father_con,
        "community_id"  : state.selectedComm,
        "username"    : state.username,
        "Password"    : state.password,
        "rePassword"  : state.repassword,
        "part_id[0]"   : state.selectedGotra[0],
        "part_id[1]"   : state.selectedGotra[1],
      }
      this.setState({
        visible: true
      });
      PostData.simplePost('Users/addRest.json',post).then((res) => {
        let responseJson = res.data;
        this.setState({
          visible: false
        });
        
        if(responseJson.response == "true"){
          this.setState({
            formvisible : false,
            message     : responseJson.text,
            color       : Colors.green.green700
          });
          try {
            UserSession.setSession(responseJson);
          }catch (error) {
            console.error('session error: ' + error.message);
          }
          this.props.navigation.navigate('Home', { login : true });
        }else if(responseJson.response == "false"){
          let obj = responseJson;
          console.log(obj);
          if(obj.Email){
            emailEr = JSON.stringify(obj.Email)
            this.setState({emailEr:emailEr});
          }

          if(obj.username){
            usernameEr = JSON.stringify(obj.username)
            console.log(obj.username);
            console.log(usernameEr);
            this.setState({usernameEr:usernameEr});
          }

          if(obj.password){
            passwordEr = JSON.stringify(obj.password)
            this.setState({passwordEr:passwordEr});
          }

          if(obj.password1){
            repasswordEr = JSON.stringify(obj.password1)
            this.setState({repasswordEr:repasswordEr});
          }

          if(obj.Detail){
            if(obj.Detail.Name){
              nameEr = JSON.stringify(obj.Detail.Name)
              this.setState({nameEr:nameEr});
            }
            if(obj.Detail.Profile){
              if(obj.Detail.Profile.CommunityGroup){
                CommErr = "Please Select At-least one Community identity (eg Gotra)"
                this.setState({CommErr:CommErr});
                if(obj.Detail.Profile.CommunityGroup.father){
                  father_conEr = JSON.stringify(obj.Detail.Profile.CommunityGroup.father)
                  this.setState({father_conEr:father_conEr});
                }
              }             
            }
          }
        }else{
          this.setState({
            formvisible : false,
            message     : "Unknown error occured",
            color       : Colors.red.red700
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
    }
  }

  cancel() {
    this.setState(initial_state);
  }

  boldText(label, required = false){
    if(required){
      return (<Text style={{fontWeight: 'bold',fontSize: 16,marginLeft:5}}>{label}: <Text style={{color:'red'}}>*</Text></Text>);
    }else{
      return (<Text style={{fontWeight: 'bold',fontSize: 16,marginLeft:5}}>{label}:</Text>);
    }
  }

  radioChanged(value){
    this.setState({gender:value});
  }

  getGotra(v,i) {
    if(v!=0){
      this.setState({selectedComm:v});
      NetInfo.isConnected.fetch().then(isConnected => {
        if(isConnected){
          this.setState({isConnected:true});
          FetchHtml.getRawData(`SubCommunities/restGetSubCommByComm/${v}.json`).then((res) => {
            let gotra = res.data.map((s, i) => {
              return (
                <View key={i}>
                  {this.boldText('Select ' + s.SubCommunity)}
                  <Picker
                    key={i}
                    selectedValue={this.state.selectedGotra[i]}
                    onValueChange={(itemValue, itemIndex) => {this.setGotra(s.SubCommunity_id,itemValue, i, v)}}>
                      <Picker.Item key={i} value='0' label={'Select '+s.SubCommunity} />
                      {this.getPick(s,i)}
                  </Picker>
                </View>
              );
            });
            this.setState({
              gotra: gotra
            });
          })
          .catch((error) => {
            console.error(error);
          });
        }else{
          this.setState({visible:false});
        }
      });
    }
  }

  setGotra(comm, gotra, index, v){
    let selGot = this.state.selectedGotra;
    selGot[index] = gotra;
    this.setState({
      selectedGotra:selGot
    });
    this.getGotra(v,index);
  }

  getPick(s,j){
    return s.dropdown.map((v, i) => {
      return (<Picker.Item key={j} value={v.Part.id} label={v.Part.Name} />)
    });
  }

  render() {
    let groupItems = this.state.community.map((s, i) => {
        return <Picker.Item key={i} value={s.Community.id} label={s.Community.Name} />
    });

    return (
      <MenuContext>
        <TopHeader {...this.props} />
        <Header
          style={Wrapper.subheader}
          statusBarProps={{ barStyle: 'light-content' }}
          centerComponent={{ text: 'Create Account', style: { color: '#fff',fontSize:23 } }}
        />
        { this.state.formvisible &&
          <ScrollView style={{height: this.state.visibleHeight,padding:5,marginBottom:30}}>
            {this.boldText('Name',true)}
            <TextField
              onChangeText={value => this.setState({name: value.trim()})}
              placeholder={'Enter Full Name'}
              onBlur={() => {
                this.setState({
                  nameEr: validate('name', this.state.name)
                })
              }}
              error={this.state.nameEr}/>

            {this.boldText('Gender',true)}
            <View style={{paddingVertical:5}}>
              <RadioForm
                radio_props={radio_props}
                initial={'M'}
                formHorizontal={true}
                onPress={(value) => {this.radioChanged(value)}}
              />
            </View>

            {this.boldText('Email',true)}
            <TextField
              onChangeText={value => this.setState({email: value.trim()})}
              placeholder={'Enter Email Id'}
              onBlur={() => {
                this.setState({
                  emailEr: validate('email', this.state.email)
                })
              }}
              error={this.state.emailEr}/>

            {this.boldText('Ancestor Place')}
            <TextField
              onChangeText={value => this.setState({ancestor: value.trim()})}
              placeholder={'Enter Ancestor Place'}
              onBlur={() => {
                this.setState({
                  ancestorEr: validate('ancestor', this.state.ancestor)
                })
              }}
              error={this.state.ancestorEr}/>

            {this.boldText('Father Name',true)}
            <TextField
              onChangeText={value => this.setState({fname: value.trim()})}
              placeholder={'Enter Father Name'}
              onBlur={() => {
                this.setState({
                  fnameEr: validate('name', this.state.fname)
                })
              }}
              error={this.state.fnameEr}/>

            {this.boldText('Father Contact No.',true)}
            <TextField
              onChangeText={value => this.setState({father_con: value.trim()})}
              placeholder={'Enter Contact No.'}
              onBlur={() => {
                this.setState({
                  father_conEr: validate('contact', this.state.father_con)
                })
              }}
              error={this.state.father_conEr}/>

            {this.boldText('Community',true)}
            <Picker
              selectedValue={this.state.selectedComm}
              onValueChange={(itemValue, itemIndex) => this.getGotra(itemValue, itemIndex)}>
              {groupItems}
            </Picker>
            {this.state.CommErr && <Text style={{color:'red'}}>Community Must be Selected</Text>}

            {this.state.gotra}

            {this.boldText('Username',true)}
            <TextField
              onChangeText={value => this.setState({username: value.trim()})}
              placeholder={'Enter username'}
              onBlur={() => {
                this.setState({
                  usernameEr: validate('username', this.state.username)
                })
              }}
              error={this.state.usernameEr}/>

            {this.boldText('Password',true)}
            <TextField
              onChangeText={value => this.setState({password: value.trim()})}
              placeholder={'Password'}
              onBlur={() => {
                this.setState({
                  passwordEr: validate('password', this.state.password)
                })
              }}
              error={this.state.passwordEr}
              secureTextEntry={true}/>

            {this.boldText('Retype-Password',true)}
            <TextField
              onChangeText={value => this.setState({repassword: value.trim()})}
              placeholder={'Retype Password'}
              onBlur={() => {
                this.setState({
                  repasswordEr: validate('repassword', this.state.repassword)
                })
              }}
              error={this.state.repasswordEr}
              secureTextEntry={true}/>

            <View style={{flexDirection:'row'}}>
              <TouchableOpacity style={{width:deviceWidth*0.5}}>
                <Button title='Register' onPress={() => this.register()}/>
              </TouchableOpacity>
              <TouchableOpacity style={{width:deviceWidth*0.5}}>
                <Button title='Cancel' color={Colors.purple.purple500} onPress={() => this.cancel()}/>
              </TouchableOpacity>
            </View>
          </ScrollView>
        }
        {
          !this.state.formvisible &&
          <View visible={!this.state.formvisible} style={{padding:5,flex:1,flexDirection: 'column',justifyContent: 'center',alignItems: 'center'}}>
            {<Text style={{fontSize:30,color:this.state.color,textAlign:'center'}}>{this.state.message}</Text>}
          </View>
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    )
  }
}

const deviceWidth = Dimensions.get('window').width;

const required  = <Text style={{color:'red'}}>*</Text>;

export default RegScreen;
