import {View, StyleSheet, Text, WebView, ScrollView,TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import FetchHtml from '../Services/FetchHtml';

import { Header, Avatar, Button } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';

import { MenuContext } from 'react-native-popup-menu';

import Wrapper from '../styles/Wrapper';

import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import Utilities from '../Components/Utilities';
import UserSession from '../Components/UserSession';

const config = require('../source/config.json');
const SITENAME = config.sitename;

const Colors = require('../source/colors.json');

import Moment from 'moment';

class FamilyScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            profile:null,
            isLogin:false,
            myProfile:false,
            token:null,
            user_id:null,
            my_id:null
        }
    }

    componentDidMount() {
        try {
            UserSession.checkSession().then((is_session) => {
                if (is_session) {
                    this.setState({isLogin: true });
                }
                try {
                    UserSession.getSession().then((users) => {
                        if(users){
                            this.setState({ token: users.token, user_id: users.user_id });
                            let state = this.props.navigation.state;
                            if (state.params && state.params.profile_id) {
                                if (state.params.profile_id == users.profile_id){
                                    this.setState({ myProfile: true, my_id: users.profile_id});
                                }
                                this.setState({ visible: true });
                                let profile_id = state.params.profile_id  
                                let url = `Profiles/restFamily/${profile_id}.json?token=${this.state.token}&user_id=${this.state.user_id}`;
                                
                                FetchHtml.getRawData(url).then((response) => {
                                    let responseJson = response.json;
                                    if(responseJson.expire && responseJson.expire == "true"){
                                        UserSession.removeSession();
                                        this.props.navigation.navigate('LoginScreen',{expire:true})
                                    }   
                                    let profile = responseJson.data;      
                                    this.setState({
                                        visible: false,
                                        profile: profile
                                    });
                                })
                                .catch ((error) => {
                                    console.error(error);
                                });
                            }
                        }                                             
                    });
                } catch (error) {
                    console.error('session error: ' + error.message);
                }
            });
        } catch (error) {
            console.error('session error: ' + error.message);
        }
    }

    getRow(u,rel,is_edit,i){
        let gender = u.Profile.gender;
        let husband = "Wife";
        let mart = "Unmarried";
        let is_account = false;
        if(u.User && u.User.id){
            is_account = true;
        }
        let color = Colors.blue.blue700;
        if(gender == 'F'){
            color = Colors.pink.pink700
        }
        if (u.Profile.mrg_status) {
            if (u.Profile.mrg_status == 'Mr') mart = 'Married';
            else if (u.Profile.mrg_status == 'Wi') {
                if (gender == 'Male') mart = 'Widower';
                else mart = 'Widow';
            } else if (u.Profile.mrg_status == 'Di') mart = 'Divorced';
            else mart = 'Unmarried';
        } else mart = 'Unmarried';

        let wife = '';
        let wifeLate = false;
        if (u.Profile.mrg_status == 'Mr'){
            if (u.WifeDetail.Name) {
                if (u.LinkedWife.late == 1) wifeLate = true
                wife += u.WifeDetail.Name
            }else{
                if (u.Wife.late == 1) wifeLate = true
                wife += u.Wife.Name
            }
        }

        return (
            <View key={i} style={styles.container}>
                <View style={styles.rowContainer}>
                    <Text style={[styles.heading,{color:color}]}>{rel}</Text>
                    {   mart!='Unmarried' && gender == 'M' && u.Profile.id != this.props.navigation.state.params.profile_id &&
                        <Button
                            onPress={() => { this.props.navigation.navigate('Family',{profile_id:u.Profile.id}); }}
                            buttonStyle={styles.btnStyle}
                            containerViewStyle={[styles.button]} 
                            title='Family / परिवार' 
                            Component={TouchableOpacity}
                            backgroundColor={Colors.indigo.indigo600}
                        />
                    }
                </View>
                <View style={{marginBottom:10}}>
                    <Text style={styles.lbl}><Text style={styles.lblh}>Name: </Text>{u.Profile.late == '1' && this.getLate()}{u.Detail.Name}</Text>
                    {mart!='Unmarried' && <Text style={styles.lbl}><Text style={styles.lblh}>{husband}: </Text>{wifeLate && this.getLate()}{wife}</Text>}
                </View>
                { this.state.myProfile && <View style={styles.rowContainer}>
                    {   mart!='Unmarried' && gender == 'M' &&
                        <Button
                            onPress={() => { this.props.navigation.navigate('AddChild', { father_id: u.Profile.id, profile_id: this.state.my_id, name: u.Detail.Name }); }}
                            buttonStyle={styles.btnStyle}
                            containerViewStyle={[styles.button,{flex:2}]} 
                            title='Add Son/Daughter'
                            Component={TouchableOpacity}
                            backgroundColor={Colors.purple.purple400}
                        />

                    }
                    {
                        <Button 
                            buttonStyle={styles.btnStyle}
                            containerViewStyle={[styles.button]} 
                            title='Edit'
                            Component={TouchableOpacity}
                            backgroundColor={Colors.purple.purple400}
                            onPress={() => { this.props.navigation.navigate('Edit', { profile_id: u.Profile.id, father_id: this.state.my_id, name: u.Detail.Name }); }}
                        />
                    }
                    { !is_account &&
                        <Button
                            onPress={() => { this.props.navigation.navigate('AddAccount', { father_id: u.Profile.id, profile_id: this.state.my_id, name: u.Detail.Name }); }}
                            buttonStyle={styles.btnStyle}
                            containerViewStyle={[styles.button]} 
                            title='Account' 
                            Component={TouchableOpacity}
                            backgroundColor={Colors.purple.purple400}
                        />
                    }
                    </View>
                }
            </View>
        );
    }

    getLate(){
        return (<Text style={{color:Colors.red.red600}}>Late </Text>)
    }

    getFamily(){
        let profile = this.state.profile;
        let father = {};
        if (profile.FatherDetail && profile.FatherDetail.id){
            father.Detail = profile.FatherDetail;
            father.Profile = profile.Father;
            console.log(father);            
        }
        return (
            <View>
                <Text style={styles.maintext}>Family of <Text style={styles.maintext2}>{profile.Detail.Name}</Text></Text>
                {this.getRow(this.state.profile,'Self',true,-1)}
                {!!father.Detail && this.getRow(father, 'Father', true, -2)}
                {this.getChild()}
            </View>

        )
    }

    getChild(){
        if(this.state.profile.Child){
            return this.state.profile.Child.map((o,i) => {
                console.log(o);                
                let rel = o.Profile.gender == 'M' ? 'Son' : 'Daughter';
                return (this.getRow(o,rel,true,i))
            });
        }
    }

    render() {
        return(
        <MenuContext>
            <Header
                leftComponent={(<Icon
                    name="long-arrow-left"
                    size={25}
                    color="white"
                    onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
                />
                )}
                style={{ ...Wrapper.subheader, paddingLeft: 5 }}
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={{ text: 'Family View', style: { color: '#fff', fontSize: 23 } }}
            />
            {this.state.profile && <ScrollView style={{marginBottom:10}}>{this.getFamily()}</ScrollView>}
            <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
        </MenuContext>
        );
    }
}

const styles = StyleSheet.create({
    maintext:{
        fontSize:18,
        fontWeight:'bold',
        textAlign:'center',
        marginBottom:10,
        marginTop:10
    },
    maintext2:{
        color:Colors.teal.teal800,
    },
    container: { 
        height:'auto',
        width: window.width,
        borderWidth:1,
        borderColor:Colors.grey.grey700,
        marginTop:10,
        padding:5
    },
    rowContainer: {
        flexDirection:'row',
        height:'auto',
    },
    button:{
        flex:1,
        marginLeft:1,
        marginRight:1,
        paddingTop:0
    },
    btnStyle:{
        padding:7
    },
    heading: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: 20,
        flex:1,
        paddingTop:0,
    },
    lbl:{
        fontWeight:'bold',
        color:'black',
    },
    lblh:{
        width:50,
        fontSize:16,
        color:Colors.grey.grey800
    },
    
});

export default FamilyScreen;
