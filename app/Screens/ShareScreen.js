import {View,StyleSheet} from 'react-native';
import React, { Component } from 'react';
import { Button } from 'react-native-elements'
import Share from 'react-native-share';

const Colors  = require('../source/colors.json');

import { MenuContext } from 'react-native-popup-menu';

import TopHeader from './TopHeader';


class ShareScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      visible: true
    }
  }

  onClick() {
    let shareOptions = {
      title: "OmerSamaj App",
      message: "Share Omersamaj App with Friends",
      url: "http://omersamaj.com/",
      subject: "Share Link" //  for email
    };

    let shareImageBase64 = {
      title: "React Native",
      message: "Hola mundo",
      subject: "Share Link" //  for email
    };
    Share.open(shareOptions).catch((err) => { err && console.log(err); })
  }
  render(){
    return (
      <MenuContext>
        <TopHeader {...this.props} />
        <View style={styles.pageContainer}>
          <Button
            large
            backgroundColor={Colors.blue.blue800}
            icon={{name: 'share'}}
            title='Share'
            color='white'
            onPress={(event) => {this.onClick()}}
          />
        </View>
      </MenuContext>
    )
  }

}

const styles = StyleSheet.create({
  pageContainer: {
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default ShareScreen;
