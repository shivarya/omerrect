import {View, StyleSheet, Text, TouchableOpacity, ToastAndroid} from 'react-native';
import React, { Component } from 'react';
import { Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import Wrapper from '../styles/Wrapper';

import UserSession from '../Components/UserSession';

import RNRestart from 'react-native-restart';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  MenuContext
} from 'react-native-popup-menu';

class TopHeader extends Component {
  constructor(props){
    super(props);
    this.state = {
      opened: false,
      loginTitle : 'Log In',
      changed:false,
      isLogin:false,
      profile_id:null
    }
  }

  goToLink(v){
    if(v == 1){
      if(this.state.loginTitle == 'Log In'){
        this.props.navigation.navigate('LoginScreen');
      }else{
        UserSession.removeSession();
        RNRestart.Restart();
      }
    }else if(v == 2){
      this.props.navigation.navigate('Registration');
    } else if (v == 3) {
      this.props.navigation.navigate('Profile',{ profile_id:this.state.profile_id});
    }
  }

  componentWillMount () {
    try {
      UserSession.checkSession().then((is_session) => {
        if(is_session){
          this.setState({loginTitle:'Logout',isLogin:true});
          try {
            UserSession.getSession().then((users) => {
              this.setState({ profile_id: users.profile_id})
            });
          } catch (error) {
            console.error('session error: ' + error.message);
          }
        }else{
          this.setState({loginTitle:'Log In',isLogin:false});
        }
      });
    }catch (error) {
      console.error('session error: ' + error.message);
    }
  }

  render(){
    return(
      <View>
        <Header
          style={Wrapper.header}
          statusBarProps={{ barStyle: 'light-content' }}
          leftComponent={(<Icon
            name="bars"
            size={25}
            color="white"
            onPress={() => {this.props.navigation.navigate('DrawerToggle')}}
            />
          )}
          centerComponent={{ text: 'OmerSamaj', style: { color: 'white', textAlign:'center',fontSize:23 } }}
          rightComponent={(
            <TouchableOpacity style={{width:30,alignItems:'center'}} onPress={() => { this.setState({opened:true})}}>
              <Icon
                name="ellipsis-v"
                size={25}
                color="#fff"              
              />
            </TouchableOpacity>
          )}
        />
        <View style={styles.pageContainer}>
          <Menu
            opened={this.state.opened}
            onSelect={value => {this.goToLink(value)}}
            onBackdropPress={() => { this.setState({opened:false})}}
          >
            <MenuTrigger/>
            <MenuOptions style={{backgroundColor:"black"}}>
              <MenuOption value={1}>
                <Text style={{color: 'white'}}>{this.state.loginTitle}</Text>
              </MenuOption>
              {!this.state.isLogin && <MenuOption value={2}>
                <Text style={{color: 'white'}}>Create Account</Text>
              </MenuOption> }
              {this.state.isLogin && <MenuOption value={3}>
                <Text style={{ color: 'white' }}>My Profile</Text>
              </MenuOption>}
            </MenuOptions>
          </Menu>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pageContainer: {
    alignSelf: 'flex-end'
  }
});

export default TopHeader;
