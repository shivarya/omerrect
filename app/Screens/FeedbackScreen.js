import React, {Component} from 'react'
import {View, Button, Text, Dimensions, Keyboard, ScrollView, TouchableOpacity} from 'react-native'

import TextField from '../Elements/FormElements/TextField'
import validate from '../Elements/Validation/ValidateWrapper'

import TopHeader from './TopHeader';
import { Header} from 'react-native-elements';

import Wrapper from '../styles/Wrapper';

import { MenuContext } from 'react-native-popup-menu';

const Colors  = require('../source/colors.json');

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import PostData from '../Services/PostData';

import Spinner from 'react-native-loading-spinner-overlay';

let radio_props = [
  {label: 'Suggestion ', value: 1 },
  {label: 'Problem ', value: 2 }
];

let initial_state = {
  visibleHeight: Dimensions.get('window').height,
  name: '',
  nameError: '',
  contact: '',
  contactError: '',
  email:'',
  emailError:'',
  suggestion:'',
  suggError:'',
  error:false,
  suggtype:"Suggestion",
  sugg_def:1,
  visible: false,
  formvisible:true
};

class FeedbackScreen extends Component {
  constructor(props) {
    super(props)

    this.state = initial_state;
  }

  componentWillMount () {
    Keyboard.addListener('keyboardWillShow', this.keyboardWillShow.bind(this))
    Keyboard.addListener('keyboardWillHide', this.keyboardWillHide.bind(this))
  }

  keyboardWillShow (e) {
    let newSize = Dimensions.get('window').height - e.endCoordinates.height
    this.setState({visibleHeight: newSize})
  }

  keyboardWillHide (e) {
    this.setState({visibleHeight: Dimensions.get('window').height})
  }

  register() {
    const emailError = validate('email', this.state.email)
    const nameError = validate('name', this.state.name)
    const contactError = validate('contact', this.state.contact)
    const suggError = validate('suggestion', this.state.suggestion)

    this.setState({
      emailError: emailError,
      nameError: nameError,
      contactError: contactError,
      suggError: suggError
    })

    if (!emailError & !nameError & !contactError & !suggError) {
      let state = this.state;
      let post = {
        "data[Feedback][Name]"  : state.name,
        "data[Feedback][contact]":  state.contact,
        "data[Feedback][email]" : state.email,
        "data[Feedback][type]"  : state.sugg_def,
        "data[Feedback][remark]": state.suggestion
      }
      this.setState({
        visible: true
      });
      PostData.simplePost('feedback/add.json',post).then((res) => {
        let responseJson = res.json;
        this.setState({
          visible: false
        });
        if(responseJson.response == true){
          this.setState({
            formvisible : false,
            message     : responseJson.data,
            color       : Colors.green.green700
          });
        }else{
          this.setState({
            formvisible : false,
            message     : responseJson.data,
            color       : Colors.red.red700
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
    }
  }

  cancel() {
    this.setState(initial_state);
  }

  boldText(label){
    return (<Text style={{fontWeight: 'bold',fontSize: 16,marginLeft:5}}>{label}:</Text>);
  }

  radioChanged(value){
    let index = value - 1;
    let lbl = radio_props[index].label;
    this.setState({suggtype:lbl});
  }

  render() {
    return (
      <MenuContext>
        <TopHeader {...this.props} />
        <Header
          style={Wrapper.subheader}
          statusBarProps={{ barStyle: 'light-content' }}
          centerComponent={{ text: 'Feedback', style: { color: '#fff',fontSize:23 } }}
        />
        { this.state.formvisible &&
          <ScrollView style={{height: this.state.visibleHeight,padding:5}}>
            {this.boldText('Name')}
            <TextField
              onChangeText={value => this.setState({name: value.trim()})}
              placeholder={'Enter Full Name'}
              onBlur={() => {
                this.setState({
                  nameError: validate('name', this.state.name)
                })
              }}
              error={this.state.nameError}/>
            {this.boldText('Contact No.')}
            <TextField
              onChangeText={value => this.setState({contact: value.trim()})}
              placeholder={'Mobile No.'}
              onBlur={() => {
                this.setState({
                  contactError: validate('contact', this.state.contact)
                })
              }}
              error={this.state.contactError}/>
            {this.boldText('Email')}
            <TextField
              onChangeText={value => this.setState({email: value.trim()})}
              placeholder={'Enter Email Id'}
              onBlur={() => {
                this.setState({
                  emailError: validate('email', this.state.email)
                })
              }}
              error={this.state.emailError}/>
            {this.boldText('Type of Feedback')}
            <View style={{paddingVertical:5}}>
              <RadioForm
                radio_props={radio_props}
                initial={1}
                formHorizontal={true}
                onPress={(value) => {this.radioChanged(value)}}
              />
            </View>
            {this.boldText(this.state.suggtype)}
            <TextField
              onChangeText={value => this.setState({suggestion: value.trim()})}
              placeholder={'Your Text here'}
              multiline={true}
              numberOfLines={6}
              onBlur={() => {
                this.setState({
                  suggError: validate('suggestion', this.state.suggestion)
                })
              }}
              error={this.state.suggError}/>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity style={{width:deviceWidth*0.5}}>
                <Button title='Register' onPress={() => this.register()}/>
              </TouchableOpacity>
              <TouchableOpacity style={{width:deviceWidth*0.5}}>
                <Button title='Cancel' color={Colors.purple.purple500} onPress={() => this.cancel()}/>
              </TouchableOpacity>
            </View>
          </ScrollView>
        }
        {
          !this.state.formvisible &&
          <View visible={!this.state.formvisible} style={{padding:5,flex:1,flexDirection: 'column',justifyContent: 'center',alignItems: 'center'}}>
            {<Text style={{fontSize:30,color:this.state.color,textAlign:'center'}}>{this.state.message}</Text>}
          </View>
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    )
  }
}
const deviceWidth = Dimensions.get('window').width;
// const styles = StyleSheet.create({
// });

export default FeedbackScreen;
