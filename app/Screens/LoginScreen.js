import React, {Component} from 'react'
import {View, Button, Text, Dimensions, Keyboard, ScrollView, TouchableOpacity, ToastAndroid} from 'react-native'

import TextField from '../Elements/FormElements/TextField'
import validate from '../Elements/Validation/ValidateWrapper'

import TopHeader from './TopHeader';
import { Header} from 'react-native-elements';

import Wrapper from '../styles/Wrapper';

import { MenuContext } from 'react-native-popup-menu';

const Colors  = require('../source/colors.json');

import PostData from '../Services/PostData';

import Spinner from 'react-native-loading-spinner-overlay';

import UserSession from '../Components/UserSession';

let initial_state = {
  visibleHeight: Dimensions.get('window').height,
  username: '',
  usernameError: '',
  password:'',
  passwordError:'',
  error:false,
  visible: false,
  formvisible:true
};

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = initial_state;
  }

  componentWillMount () {
    let state = this.props.navigation.state;
    if (state.params && state.params.expire) {
      ToastAndroid.show("Token Expired,Login Again",ToastAndroid.LONG);
    }
  }

  login() {
    const usernameError = validate('username', this.state.username)
    const passwordError = validate('password', this.state.password)

    this.setState({
      usernameError: usernameError,
      passwordError: passwordError
    })

    if (!usernameError & !passwordError) {
      let state = this.state;
      let post = {
        "username"  : state.username,
        "password":  state.password
      }
      this.setState({
        visible: true
      });
      PostData.simplePost('users/userLogin.json',post).then((res) => {
        let responseJson = res.data;
        this.setState({
          visible: false
        });
        if(responseJson.response == "true"){
          this.setState({
            formvisible : false,
            message     : "Login Success",
            color       : Colors.green.green700
          });
          try {
            UserSession.setSession(responseJson);
          }catch (error) {
            console.error('session error: ' + error.message);
          }
          this.props.navigation.navigate('Home', { login : true });
        }else{
          this.setState({
            formvisible : false,
            message     : responseJson.text,
            color       : Colors.red.red700
          });
        }
      })
      .catch((error) => {
        console.error(error);
      });
    }
  }

  boldText(label){
    return (<Text style={{fontWeight: 'bold',fontSize: 16,marginLeft:5}}>{label}:</Text>);
  }

  render() {
    return (
      <MenuContext>
        <TopHeader {...this.props} />
        <Header
          style={Wrapper.subheader}
          statusBarProps={{ barStyle: 'light-content' }}
          centerComponent={{ text: 'Login', style: { color: '#fff',fontSize:23 } }}
        />
        { this.state.formvisible &&
          <ScrollView style={{height: this.state.visibleHeight,padding:5}}>
            {this.boldText('Username')}
            <TextField
              onChangeText={value => this.setState({username: value.trim()})}
              placeholder={'Enter username'}
              onBlur={() => {
                this.setState({
                  usernameError: validate('username', this.state.username)
                })
              }}
              error={this.state.usernameError}/>
            {this.boldText('Password')}
            <TextField
              onChangeText={value => this.setState({password: value.trim()})}
              placeholder={'Password'}
              onBlur={() => {
                this.setState({
                  passwordError: validate('password', this.state.password)
                })
              }}
              error={this.state.passwordError}
              secureTextEntry={true}/>
            <TouchableOpacity>
              <Button title='Login' onPress={() => this.login()}/>
            </TouchableOpacity>
          </ScrollView>
        }
        {
          !this.state.formvisible &&
          <View style={{padding:5,flex:1,flexDirection: 'column',justifyContent: 'center',alignItems: 'center'}}>
            {<Text style={{fontSize:30,color:this.state.color,textAlign:'center'}}>{this.state.message}</Text>}
          </View>
        }
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{color: '#FFF'}} />
      </MenuContext>
    )
  }
}

export default LoginScreen;
