import React, {Component} from 'react'
import { 
  View, 
  Text, 
  Dimensions, 
  Keyboard, 
  ScrollView, 
  TouchableOpacity, 
  TextInput, 
  AsyncStorage,
  StyleSheet,
  Image
} from 'react-native'

import { Card, ListItem, Button, Header } from 'react-native-elements'

import Wrapper from '../styles/Wrapper';

import { MenuContext } from 'react-native-popup-menu';

const Colors  = require('../source/colors.json');

import Spinner from 'react-native-loading-spinner-overlay';

import { NavigationActions } from 'react-navigation'

import Icon from 'react-native-vector-icons/FontAwesome';

import Filter from '../Components/Filter'
import Modal from 'react-native-modal';

import Autocomplete from 'react-native-autocomplete-input';

import FetchHtml from '../Services/FetchHtml';

const config = require('../source/config.json');
const SITENAME = config.sitename;

let initial_state = {
  visibleHeight: Dimensions.get('window').height,
  visible:false,
  isModalVisible: false,
  names:[],
  nameIds:[],
  community_id:0,
  query:'',
  users:[],
  curPage:0,
  size:1,
  filterJson:null,
  loadmore:false,
  result:'Type to Start search',
  is_mrg:false,
  title:'Genearl Search'
};


class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = initial_state;
  }

  componentWillMount () {
    let state = this.props.navigation.state;
    if (state.params && state.params.is_mrg) {
      this.setState({is_mrg: true,title:'Matrimony Search'});
    }
    Keyboard.addListener('keyboardWillShow', this.keyboardWillShow.bind(this))
    Keyboard.addListener('keyboardWillHide', this.keyboardWillHide.bind(this))
    AsyncStorage.getItem('comm').then((comm) => {
      if(comm != null){
        let c = parseInt(comm);
        this.setState({community_id: c});
      }
    });
    AsyncStorage.removeItem('filters');
    AsyncStorage.getItem('filters').then((filters) => {
      let res = JSON.parse(filters);
      let filterReset = {};      
      for (var key in res) {
        filterReset[key] = {};
      }      
      AsyncStorage.setItem('filters', JSON.stringify(filterReset));
    });
    this.setState({users:[]});
  }

  keyboardWillShow (e) {
    let newSize = Dimensions.get('window').height - e.endCoordinates.height
    this.setState({visibleHeight: newSize})
  }

  keyboardWillHide (e) {
    this.setState({visibleHeight: Dimensions.get('window').height})
  }

  _showModal = () => this.setState({ isModalVisible: true })

  _hideModal(){
    this.setState({ isModalVisible: false });
    this._search();
  }

  _findNames(text){
    this.setState({ query: text, names:[],nameIds:[] });
    FetchHtml.getRawData(`Search/getHint.json?query=${text}&community_id=${this.state.community_id}`).then((res) => {
        let responseJson = res.json;        
        let names = [];
        let nameIds = [];
        responseJson.map((o, i) => {
          names.push(o.name);
          nameIds.push(o.id);
        });             
        this.setState({
          names: names,
          nameIds: nameIds
        });
    })
    .catch((error) => {
        console.error(error);
    });  
  }

  _search(){
    this.setState({ visible: true, users:[] }); 
    AsyncStorage.getItem('filters').then((filters) => {     
      let res = JSON.parse(filters);
      let json = {};
      for (var k in res) {
        for(i in res[k]){
          if(res[k][i] == true){
            json[k] = i;
            continue;
          }
        }
      }
      if(!json['community_id']){
        json['community_id'] = this.state.community_id;
      }
      json['query'] = this.state.query;
      this.setState({ filterJson:json,curPage:0});
      this.fetchData(json);         
    });
  }

  loadMore() {
    this.setState({ visible: true });
    this.fetchData(this.state.filterJson);
  }

  fetchData(json){
    let curPage = this.state.curPage + 1;
    json['page'] = curPage;
    let state = this.props.navigation.state;
    let url = `search/restSearch.json`;
    if (state.params && state.params.is_mrg) {
      url+=`?is_mrg=1`;
    }
    FetchHtml.getDataGET(url, json).then((res) => {
      let responseJson = res.jsonData;
      let users = this.state.users;
      let total = responseJson.total;
      let size = Math.ceil(total / responseJson.perpage);
      if (size == curPage || size == 0){
        this.setState({ loadmore:false});
      }else{
        this.setState({ loadmore: true }); 
      }
      let result = null;
      if (total > 0){
        result = `Total ${total} Results found`;
      }else{
        result = `No Resullt Found`;        
      }
      console.log(responseJson.data);
      
      users.push.apply(this.state.users, responseJson.data);
      this.setState({
        curPage: curPage,
        users: users,
        visible: false,
        result:result,
        size:size
      });
    })
    .catch((error) => {
      console.error(error);
    });
  }

  goToProfile(text){
    this.setState({ query: text });
    let names = this.state.names;
    let nameIds = this.state.nameIds;
    let index = names.indexOf(text);
    if(index != -1){
      let id = nameIds[index];
      this.props.navigation.navigate('Profile', { profile_id: id });
    }
    
  }

  render() {
    return (
      <MenuContext>
        <Header
          leftComponent={(<Icon
            name="long-arrow-left"
            size={25}
            color="white"
            onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
          />
          )}
          style={{...Wrapper.subheader,paddingLeft:5}}
          statusBarProps={{ barStyle: 'light-content' }}
          centerComponent={{ text: this.state.title, style: { color: '#fff',fontSize:23 } }}
        />
        <View 
          style={{
            flexDirection:'row',
            width: window.width, 
            margin: 10, 
            alignItems:'center', 
            justifyContent:'center'
          }}
        >
          <View style={{flex:4}}>
            <Autocomplete
              data={this.state.names}
              placeholder="Enter Text to Search"
              onChangeText={text => this._findNames(text)}
              containerStyle={styles.autocompleteContainer}
              listContainerStyle={styles.listContainerStyle}
              listStyle={styles.listStyle}
              renderItem={data => (
                <TouchableOpacity onPress={() => {this.goToProfile(data)}}>
                  <Text style={styles.itemText}>{data}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View style={{ flex: 2, flexDirection: 'row'}}>
            <Button
              onPress={() => this._search()} 
              icon={{ name: 'search', size: 20 }} 
              buttonStyle={{ paddingRight: 0, paddingLeft: 8}} 
              backgroundColor={Colors.blue.blue800} 
              containerViewStyle={{ width: 50}} 
            />
            <Button 
              onPress={this._showModal} 
              icon={{ name: 'filter', size: 20, type: 'font-awesome' }} 
              buttonStyle={{ paddingRight: 0, paddingLeft: 8 }} 
              backgroundColor={Colors.blue.blue800} 
              containerViewStyle={{ width: 50, marginLeft: -10}} 
            />
          </View>
        </View>
        <ScrollView>
          {this.state.result && <Text style={{fontSize:20,color:Colors.purple.purple800,marginLeft:10}}>{this.state.result}</Text>}
          {this.state.users.length > 0 && <Card>{ 
            this.state.users.map((u, i) => {
              let image = require('../source/img/profile-pic.jpg');
              let gender = u.Profile.gender == "M" ? 'Male' : 'Female';
              if(u.Profile.gender == 'F'){
                image = require('../source/img/fe-avatar.jpg')
              }              
              if(u.Detail.Detail && u.Detail.Detail.ProfilePic.image){
                var timeStamp = Math.floor(Date.now() / 1000); 
                uri = `${SITENAME}files/profile_pic/image/${u.Detail.id}/${u.Detail.Detail.ProfilePic.image}?dummy=${timeStamp}`;
                image = {uri : uri};
              }
              let father = '';
              if(u.Father.Detail && u.Father.Detail.Name){
                  if(u.Father.late == 1) father = <Text color={Colors.red.red500}>Late </Text>
                  father += u.Father.Detail.Name;
              }else{
                  father = u.CommunityGroup.father;
              }

              let mart = "Unmarried";
              if(u.Profile.mrg_status){
                if(u.Profile.mrg_status == 'Mr') mart = 'Married';
                else if(u.Profile.mrg_status == 'Wi'){
                    if(gender=='Male') mart = 'Widower';
                    else mart = 'Widow';
                }else if(u.Profile.mrg_status == 'Di') mart = 'Divorced';
                else mart = 'Unmarried';
              }else  mart = 'Unmarried';

              let community = '';
              if(u.CommunityGroup.Community.Name){
                community = u.CommunityGroup.Community.Name;
              }

              return (
                <TouchableOpacity 
                  key={i} 
                  style={styles.user}
                  onPress={() => { this.props.navigation.navigate('Profile', { profile_id: u.Detail.id })}}
                >
                  <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={image}
                  />
                  <View style={styles.profile}>
                    <View style={styles.profileInfoRow}>
                      <Text style={styles.profileInfoName}> Name: </Text>
                      <Text style={styles.profileInfoValue}>{u.Detail.Name}</Text> 
                    </View>
                    <View style={styles.profileInfoRow}>
                      <Text style={styles.profileInfoName}> Gender: </Text>
                      <Text style={styles.profileInfoValue}>{gender}</Text>
                    </View>
                    <View style={styles.profileInfoRow}>
                      <Text style={styles.profileInfoName}> Marital Status: </Text>
                      <Text style={styles.profileInfoValue}>{mart}</Text>
                    </View>
                    <View style={styles.profileInfoRow}>
                      <Text style={styles.profileInfoName}>Father: </Text>
                      <Text style={styles.profileInfoValue}>{father}</Text>
                    </View>
                    <View style={styles.profileInfoRow}>
                      <Text style={styles.profileInfoName}> Community: </Text>
                      <Text style={styles.profileInfoValue}>{community}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })
          }
          </Card>}
          { this.state.loadmore && 
            <TouchableOpacity 
              style={styles.loadBtn}
              onPress={() => this.loadMore()}
            >            
              <Icon
                name='repeat'
                size={30}
                color="#fff"
              />
              <Text style={{color:'white',fontSize:20,paddingLeft:8}}>Load More</Text>
            </TouchableOpacity>
          }
        </ScrollView>
        <Modal 
          isVisible={this.state.isModalVisible} style={{backgroundColor:'white'}}
          onBackdropPress={() => this.setState({ isModalVisible: false })}
        >
          <Filter is_mrg={this.state.is_mrg} />
          <Button
            onPress={() => {this._hideModal()}}
            title='Apply Filters' 
            icon={{ name: 'check', size: 20 }} 
            backgroundColor={Colors.blue.blue800}
            containerViewStyle={{paddingBottom:5}}
          />
        </Modal>
        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
      </MenuContext>
    );
  }
}

const styles = StyleSheet.create({
  autocompleteContainer: {
    left: 0,
    position: 'absolute',
    right: 0,
    top: -20,
    zIndex: 1
  },
  loadBtn:{
    flexDirection: 'row',
    width: window.width,
    margin: 10,
    borderBottomWidth: 1,
    padding: 8,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: Colors.purple.purple700 
  },
  itemText: {
    fontSize: 15,
    color:'black'
  },
  listContainerStyle :{
    width:'110%',
    left:-10
  },
  listStyle:{
    paddingLeft:10
  },
  user:{
    flexDirection: 'row',  
    width: window.width,
    margin: 1,
    borderBottomWidth: 1,
    paddingBottom:5
  },
  image:{
    flex:2,
    height:70,
    padding:1,
    borderWidth:0.5,
    borderColor:'#ccc'
  },
  profile:{
    flex:5,
    marginLeft:2,
    borderColor: '#DCEBF7'
  },
  profileInfoRow:{
    flexDirection: 'row'
  },
  profileInfoName:{
    fontSize: 10,
    paddingRight:1,
    color: '#336199',
    backgroundColor: '#EDF3F4',
    textAlign:'right',
    flex:2
  },
  profileInfoValue:{
    fontSize: 11,
    flex:3,
    paddingLeft:5
  }
});


export default LoginScreen;
