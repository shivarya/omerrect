import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    NetInfo,
    AsyncStorage
} from 'react-native';

import Panel from './Panel';

import { Button,CheckBox  } from 'react-native-elements'

const Colors = require('../source/colors.json');

import FetchHtml from '../Services/FetchHtml';

import Spinner from 'react-native-loading-spinner-overlay';

class Filter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: false,
            filters: [],
            visible:false,
            filterRes:{}
        }
    }

    componentWillMount() {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                this.setState({ isConnected: true, visible: false });
                let url = 'search/restHeading.json';                
                if (this.props.is_mrg) {                    
                    url+=`?is_mrg=1`;
                }
                FetchHtml.getRawData(url).then((res) => {
                    let responseJson = res.jsonData;
                    AsyncStorage.getItem('filters').then((filters) => {
                        let res = JSON.parse(filters);
                        this.setState({ filterRes: res });
                        responseJson.map((o, i) => {
                            let id = o.key;
                            let filterRes = this.state.filterRes;                            
                            if (!this.state.filterRes[id]) {
                                if(id == "community_id"){
                                    filterRes[id] = {};
                                    this.setState({ filterRes: filterRes });
                                    AsyncStorage.getItem('comm').then((comm) => {
                                        filterRes['community_id'][comm] = true;
                                        if(comm != null){
                                            for (var comm_id in filterRes['community_id']) {
                                                if(comm_id == comm){
                                                    filterRes['community_id'][comm_id] = true;
                                                }else{
                                                    filterRes['community_id'][comm_id] = false;
                                                }
                                            }                                            
                                        }
                                        this.setState({ filterRes: filterRes });
                                    });
                                }else{
                                    filterRes[id] = {};
                                    this.setState({ filterRes: filterRes });
                                }
                                
                            }
                        });
                        this.setState({
                            filters: responseJson,
                            visible:false
                        });
                    });
                })
                .catch((error) => {
                    console.error(error);
                }); 
            }
        });
    }

    _getBox(obj,id){
        return obj.map((o,i) => {
            let key = o.k;
            return(<CheckBox
                key={i}
                title={o.v}
                checked={this.state.filterRes[id][key]}
                onIconPress={() => {this._toggleCheckBox(id,key)}}
            />);
        });
    }

    _toggleCheckBox(id,key){
        let curState = this.state.filterRes[id][key];        
        let newSatate = curState ? false : true;
        let filterRes = this.state.filterRes;  
        filterRes[id][key] = newSatate;
        for (var k in filterRes[id]) {
            if(k == key && newSatate){
                filterRes[id][k] = true;
            }else{
                filterRes[id][k] = false;
            }
        }
        this.setState({ filterRes: filterRes});
        try {
            AsyncStorage.setItem('filters', JSON.stringify(filterRes));     
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }
    
    render(){
        let filterUi = this.state.filters.map((o, i) => {
            let tempArray = [];
            let obj = o.ary;
            for (const prop in obj) {
                let tob = { k: prop, v: obj[prop] };
                tempArray.push(tob);
            }
            let id = o.key;
            return (<Panel key={i} title={o.Name}>
                {this._getBox(tempArray,id)}
            </Panel>);
        });
        return (
            <ScrollView >
                {filterUi} 
                <Spinner visible={true} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />               
            </ScrollView>
        )
    }
}

export default Filter;