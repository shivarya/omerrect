import { AsyncStorage } from 'react-native';
import React from 'react';

const UserSession = {
  setSession    : setSession,
  getSession    : getSession,
  checkSession  : checkSession,
  removeSession : removeSession,
  getCommunity  : getCommunity
}

async function setSession(user_data) {
  try {
    await AsyncStorage.setItem('users', JSON.stringify(user_data));
  } catch (error) {
    console.error('AsyncStorage error: ' + error.message);
  }
}

async function getSession() {
  let response = await AsyncStorage.getItem('users').then((users) => {
    return JSON.parse(users);
  });
  return response;
}

async function checkSession() {
  let response = await AsyncStorage.getItem('users').then((users) => {
    return users;
  });
  if(response){
    return true;
  }else{
    return false;
  }
}

async function getCommunity() {
  let response = await AsyncStorage.getItem('comm').then((comm) => {
    return comm;
  });
  return response;
}

function removeSession() {
  AsyncStorage.removeItem('users');
}

export default UserSession;
