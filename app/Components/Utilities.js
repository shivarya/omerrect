import { AsyncStorage } from 'react-native';
import React from 'react';

const Utilities = {
    jsonToQueryString   : jsonToQueryString,
    makeAddress: makeAddress,
    makeContact: makeContact
}

function jsonToQueryString(source){
    let array = [];

    for(var key in source) {
        array.push(encodeURIComponent(key) + "=" + encodeURIComponent(source[key]));
    }

    return array.join("&");
}

function makeAddress(addArray = {}) {
    if (addArray) {
        let address = [];
        if (addArray.add_line1) address.push(addArray.add_line1);
        if (addArray.add_line2) address.push(addArray.add_line2);
        if (addArray.Area) address.push(addArray.Area);
        if (addArray.Area && addArray.Area.City) address.push(addArray.Area.City);
        return address.join(", ");
    }
    return "";
}

function makeContact(contArray = {}, std = "") {
    if (contArray) {
        let contact = [];
        if (contArray.landline) {
            let landline = contArray.landline
            if(std.length > 0){
                landline += `(${std}) ${contArray.landline}`;
            }
            contact.push(landline);
        }
        if (contArray.mobile){
            let mobile = `Mob. - ${contArray.mobile}`;
            contact.push(mobile);
        }

        return contact.join(", ");
    }
    return "";
}

export default Utilities;
