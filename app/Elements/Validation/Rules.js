const Rules = {
  email: {
    format: {
      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      message: '^Please enter a valid email address'
    },
    presence: {
      message: '^Please enter Email'
    }
  },
  name: {
    presence: {
      message: '^Please enter a Name'
    }
  },
  contact:{
    presence: {
      message: '^Please enter a Contact'
    },
    numericality: {
      onlyInteger: true,
      message: '^Please enter a Valid Contact'
    }
  },
  suggestion:{
    presence: {
      message: '^Please enter a Text'
    },
    length: {
      minimum: 3,
      maximum:100,
      tooShort: "needs to have %{count} words or more",
      tooLong: "must not be grater than %{count} chars"
    }
  },
  ancestor: {
    length: {maximum: 100}
  },
  community: {
    presence: {
      message: '^Please Select a Community'
    },
    exclusion: {
      within: [0],
      message: "^Please Select a Community"
    },
    numericality: true
  },
  username: {
    presence: {
      message: '^Please enter a Username'
    }
  },
  password: {
    presence: {
      message: '^Please enter a Password'
    },
    exclusion: {
      within: [""],
      message: "^Please Enter Password"
    },
  },
  repassword: {
    equality:  "password"
  }
}

export default Rules;
