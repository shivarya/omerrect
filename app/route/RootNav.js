import React, { Component } from 'react';
import { StyleSheet, View, Text} from 'react-native';
import { DrawerNavigator, DrawerItems, StackNavigator } from 'react-navigation';

import HomeScreen from '../Screens/HomeScreen';
import AboutScreen from '../Screens/AboutScreen';
import ContactScreen from '../Screens/ContactScreen';
import FeedbackScreen from '../Screens/FeedbackScreen';
import TermNConScreen from '../Screens/TermNConScreen';
import DisclaimerScreen from '../Screens/DisclaimerScreen';
import PrivacyScreen from '../Screens/PrivacyScreen';
import ShareScreen from '../Screens/ShareScreen';
import LoginScreen from '../Screens/LoginScreen';
import RegScreen from '../Screens/RegScreen';

import SideMenu from '../Screens/SideMenu';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchScreen from '../Screens/SearchScreen';
import ProfileScreen from '../Screens/ProfileScreen';
import CategoryScreen from '../Screens/CategoryScreen';
import SubMenuScreen from '../Screens/SubMenuScreen';
import CatViewScreen from '../Screens/CatViewScreen';
import HelpViewScreen from '../Screens/HelpViewScreen';
import HelpUsersScreen from '../Screens/HelpUsersScreen';
import TalentUsersScreen from '../Screens/TalentUsersScreen';
import JobViewScreen from '../Screens/JobViewScreen';
import OrgViewScreen from '../Screens/OrgViewScreen';
import OrgScreen from '../Screens/OrgScreen';
import NewsScreen from '../Screens/NewsScreen';
import CameraScreen from '../Screens/CameraScreen';
import FamilyScreen from '../Screens/FamilyScreen';
import AddChildScreen from '../Screens/AddChildScreen';
import AddAccountScreen from '../Screens/AddAccountScreen';
import EditScreen from '../Screens/EditScreen';

const SideNav = DrawerNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      drawerLabel: 'Home',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-home' : 'ios-home-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
  About: {
    screen: AboutScreen,
    navigationOptions: {
      drawerLabel: 'About Us',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-contacts' : 'ios-contacts-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
  Contact: {
    screen: ContactScreen,
    navigationOptions: {
      drawerLabel: 'Contact Us',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-call' : 'ios-call-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
  Feedback: {
    screen: FeedbackScreen,
    navigationOptions: {
      drawerLabel: 'Feedback',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-chatbubbles' : 'ios-chatbubbles-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
  TermNCon: {
    screen: TermNConScreen,
    navigationOptions: {
      drawerLabel: 'Term & Conditions',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-clipboard' : 'ios-clipboard-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
  Disclaimer: {
    screen: DisclaimerScreen,
    navigationOptions: {
      drawerLabel: 'Disclaimer',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-warning' : 'ios-warning-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
  Privacy: {
    screen:PrivacyScreen,
    navigationOptions: {
      drawerLabel: 'Privacy Policy',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-lock' : 'ios-lock-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
  Share: {
    screen: ShareScreen,
    navigationOptions: {
      drawerLabel: 'Share App',
      drawerIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-share' : 'ios-share-outline'}
          size={20}
          style={{ color: tintColor }}
        />
      ),
    },
  },
},
{
  contentComponent:SideMenu
});

const RootNav = StackNavigator({
  SideNav : {
    screen:SideNav
  },
  LoginScreen : {
    screen:LoginScreen
  },
  Registration : {
    screen:RegScreen
  },
  Search : {
    screen:SearchScreen
  },
  Profile: {
    screen: ProfileScreen
  }, 
  Category:{
    screen: CategoryScreen
  },
  SubMenu: {
    screen: SubMenuScreen
  },
  CatView:{
    screen:CatViewScreen
  },
  HelpView:{
    screen:HelpViewScreen
  },
  HelpUsers:{
    screen:HelpUsersScreen
  },
  TalentUsers:{
    screen:TalentUsersScreen
  },
  JobView:{
    screen:JobViewScreen
  },
  OrgView:{
    screen:OrgViewScreen
  },
  Org:{
    screen:OrgScreen
  },
  News:{
    screen:NewsScreen
  },
  Camera:{
    screen:CameraScreen
  },
  Family:{
    screen:FamilyScreen
  },
  AddChild:{
    screen:AddChildScreen
  },
  AddAccount:{
    screen:AddAccountScreen
  },
  Edit:{
    screen:EditScreen
  }
},{
  headerMode: 'none',
});

export default RootNav;
