import React, { Component } from 'react';
import { ToastAndroid, NetInfo } from 'react-native';
import RootNav from './app/route/RootNav';

export default class OmerReact extends Component {
  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if(!isConnected){
        ToastAndroid.show(`Please Connect to internet to Start`, ToastAndroid.SHORT);
      }
    });
  }
  render(){
    return(
      <RootNav />
    );
  }
}
